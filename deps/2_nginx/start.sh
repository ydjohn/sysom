#!/bin/bash
start_app() {
    result=$(systemctl is-active nginx.service)
    if [ "$result" != "active" ]; then
        systemctl start nginx.service
    fi
}

start_app
