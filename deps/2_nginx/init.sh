#!/bin/bash
BaseDir=$(dirname $(readlink -f "$0"))

init_app() {
    mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.bak
    cp nginx.conf /etc/nginx/
    cp sysom.conf /etc/nginx/conf.d/
    ###change the install dir base on param $1###
    sed -i "s;SERVER_PORT;${SERVER_PORT};g" /etc/nginx/conf.d/sysom.conf
    sed -i "s;/usr/local/sysom;${APP_HOME};g" /etc/nginx/conf.d/sysom.conf

    # Restart nginx services
    systemctl restart nginx
}

init_app

# Start
bash -x $BaseDir/start.sh