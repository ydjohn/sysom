#!/bin/bash
start_app() {
    result=$(systemctl is-active grafana-server.service)
    if [ "$result" != "active" ]; then
        systemctl start grafana-server.service
    fi
}

start_app
