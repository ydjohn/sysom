#!/bin/bash
BaseDir=$(dirname $(readlink -f "$0"))
SERVICE_NAME=sysom-prometheus
ARCH=`uname -m`
PROMETHEUS_VER=2.29.1
PROMETHEUS_ARCH=linux-amd64
if [ "${ARCH}" == "aarch64" ]
then
    PROMETHEUS_ARCH=linue-arm64
fi
PROMETHEUS_PKG=prometheus-${PROMETHEUS_VER}.${PROMETHEUS_ARCH}
PROMETHEUS_TAR=$PROMETHEUS_PKG.tar.gz
OSS_URL=https://sysom.oss-cn-beijing.aliyuncs.com/monitor
PROMETHEUS_DL_URL=https://github.com/prometheus/prometheus/releases/download/v${PROMETHEUS_VER}

##download and install prometheus
install_prometheus()
{
    echo "install prometheus......"
    pushd ${DEPS_HOME}

    rm -rf prometheus

    ls | grep ${PROMETHEUS_TAR} 1>/dev/null 2>/dev/null
    if [ $? -ne 0 ]
    then
        rpm -q --quiet wget || yum install -y wget
        wget ${OSS_URL}/${PROMETHEUS_TAR} || wget ${PROMETHEUS_DL_URL}/${PROMETHEUS_TAR}
        ls
    fi
    tar -zxvf ${PROMETHEUS_TAR}

    ##rename the prometheus directory
    mv ${PROMETHEUS_PKG} prometheus
    popd
}

install_app() {
    install_prometheus
}

install_app
