#!/bin/bash
BaseDir=$(dirname $(readlink -f "$0"))

uninstall_app() {
    rm -rf ${DEPS_HOME}/prometheus
    rm -f ${DEPS_HOME}/prometheus*.tar.gz
}

uninstall_app