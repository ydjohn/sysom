-- 获取 KEY, FIELD, EXPIRE
local field = ARGV[1]
local expire = ARGV[2]

-- 获取当前时间（以秒为单位）
local time = redis.call('TIME')
local expire_time = tonumber(time[1]) + tonumber(time[2]) / 1000000 + expire

if expire ~= -1 then
    -- 保存过期时间
    redis.call("HSET", KEYS[2], field, expire_time)
else
    -- 删除过期限制
    redis.call("HDEL", KEYS[2], field)
end
return "OK"