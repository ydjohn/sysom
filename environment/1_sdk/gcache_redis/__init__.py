# -*- coding: utf-8 -*- #
"""
Time                2023/4/28 15:07
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                __init__.py.py
Description:
"""

name = "gcache_redis"

from .redis_gcache import *
