# -*- coding: utf-8 -*- #
"""
Time                2022/11/14 14:32
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                schemas.py
Description:
"""
from pydantic import BaseModel
from datetime import datetime

###########################################################################
# Define schemas here
###########################################################################

# @reference https://fastapi.tiangolo.com/zh/tutorial/response-model/
# class Person(BaseModel):
#     id: int
#     name: str
#     age: int
#     created_at: datetime
    
#     class Config:
#         orm_mode = True