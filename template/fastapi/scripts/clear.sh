#!/bin/bash
BaseDir=$(dirname $(readlink -f "$0"))
SERVICE_NAME=$TEMPLATE_SERVICE_NAME_MIDDLE

clear_app() {
    rm -rf /etc/supervisord.d/${SERVICE_NAME}.ini
    ###use supervisorctl update to stop and clear services###
    supervisorctl update
}

# Stop first
bash -x $BaseDir/stop.sh

clear_app
