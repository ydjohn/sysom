import GrafanaWrap from '../../Monitor/grafana'
/**
 * 进程应用观测
 * @returns 
 */
const AppObservableProcess = (props) => {
    const queryParams = !!props.location?.query ? props.location.query : {};
    // queryParams to query string
    const queryStr = Object.keys(queryParams).map(key => key + '=' + queryParams[key]).join('&').trim();
    let targetUrl = '/grafana/d/FP_k0bqVz/process_app'
    if (queryStr.length > 0) {
        targetUrl += `?${queryStr}`
    }
    return (
        <div>
            <GrafanaWrap src={targetUrl} />
        </div>
    )
};
export default AppObservableProcess;