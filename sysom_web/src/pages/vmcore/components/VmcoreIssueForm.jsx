import { Button, message } from "antd";
import { ModalForm, ProFormTextArea } from "@ant-design/pro-form";
import { PlusOutlined } from "@ant-design/icons";
import { addIssue } from "../service"; 
import { useIntl, FormattedMessage } from 'umi';

const handleAddIssue = async (fields) => {
  const intl = useIntl();
  const hide = message.loading("正在添加");

  try {
    await addIssue({ ...fields });
    hide();
    message.success("添加成功");
    return true;
  } catch (error) {
    hide();
    return false;
  }
};

const VmcoreIssue = () => {
  return (
    <ModalForm
      title={intl.formatMessage({
        id: 'pages.vmcore.inputsolution',
        defaultMessage: 'Input solution',
      })}
      trigger={
        <Button type="primary">
          <PlusOutlined />
          <FormattedMessage id="pages.vmcore.inputsolution" defaultMessage="Input solution" />
        </Button>
      }
      submitter={{
        searchConfig: {
          submitText: <FormattedMessage id="pages.template.yes" defaultMessage="Confirm" />,
          resetText: <FormattedMessage id="pages.template.no" defaultMessage="Cancel" />,
        },
      }}
      onFinish={async (value) => {
        await handleAddIssue(value);
        return true;
      }}
    >
      <ProFormTextArea label={intl.formatMessage({
        id: 'pages.vmcore.solution',
        defaultMessage: 'Solution',
      })} name="solution" />
    </ModalForm>
  );
};
export default VmcoreIssue;
