import { useState } from 'react';
import { Modal, Tooltip, Empty } from 'antd';

/*
 * 展示字段详情
 * @param tableText 表格字段显示内容
 * @param detail 需要展示的详情内容
 * @param title 展示Modal框的标题
 * @param isEllipsis 表格显示字段是否因太长, 使用省略号显示
 */
const CustomDetail = (props) => {
    const { tableText, detail, title, isEllipsis } = props;
    const [isOpenDetailModal, setIsOpenDetailModal] = useState(false);
    const openDetailModalEvent = () => { setIsOpenDetailModal(true) }
    const closeDetailModalEvent = () => { setIsOpenDetailModal(false) }
    const style = { width: '50px', whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis', cursor: 'pointer', margin: '0px', padding: '0px'}

    return (<>
        <Tooltip placement="top" title={'点击查看详情'}>
        <p 
            style={isEllipsis ? style : {cursor: 'pointer', margin: '0px', padding: '0px'} } onClick={openDetailModalEvent}>
            { tableText }
        </p>
        </Tooltip>
        <Modal
            title={title}
            open={isOpenDetailModal}
            onOk={closeDetailModalEvent}
            onCancel={closeDetailModalEvent}
            footer={null}
        >
            <p>
                { detail.length >0 ? detail : <Empty description={"暂无内容"} /> }
            </p>
        </Modal>
    </>)
}

export default CustomDetail;
