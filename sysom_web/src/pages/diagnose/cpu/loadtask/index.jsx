import { PageContainer } from '@ant-design/pro-layout';
import { Modal } from "antd";
import React, { useState, useRef, useEffect } from 'react';
import ReactDOM from 'react-dom';
import { useIntl, FormattedMessage } from 'umi';
import ProCard from '@ant-design/pro-card';
import CPUTaskForm from './CPUTaskForm';
import CPUTaskList from './CPUTaskList';
import CPUPieChart from './CPUPieChart';
import CPUEvent from './CPUEvent';
import { getTask } from '../../service'

const { Divider } = ProCard;

class SmartIFrame extends React.Component {
  render() {
      return <iframe src={this.props.src}
                     scrolling="no"
                     frameBorder={0}
                    
                     onLoad = {e => setTimeout(() => {
                         const obj = ReactDOM.findDOMNode(this);
                         obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
                         obj.style.width = obj.contentWindow.document.body.scrollWidth + 'px';
                     }, 50)}/>
  }
}

export default (props) => {
  const intl = useIntl();
  const refOSCheckList = useRef();
  const [CPUTaskResult, setCPUTaskResult] = useState();

  const onPostTask = () => {
    refOSCheckList.current.reload();
  }

  const onError = async (record) => {
    const msg = await getTask(record.task_id);
    Modal.error({
      title: <FormattedMessage id="pages.diagnose.diagnosisfailure" defaultMessage="Diagnosis failure" />,
      content: (
        <div>
          <div><FormattedMessage id="pages.diagnose.errormessage" defaultMessage="Error message:" />{msg.result}</div>
        </div>
      ),
    });
  }

  const onListClick = async (record) => {
    const msg = await getTask(record.task_id);
    setCPUTaskResult(msg);
  }

  let svgUrl = ""
  if (!!CPUTaskResult && CPUTaskResult.task_id) {
    svgUrl = `/api/v1/tasks/${CPUTaskResult.task_id}/svg/`
  }

  return (
    <PageContainer>
      <CPUTaskForm onSuccess={onPostTask} />
      <Divider />
      <CPUTaskList headerTitle={intl.formatMessage({
          id: 'pages.diagnose.viewdiagnosisrecord',
          defaultMessage: 'Diagnosis record viewing',
        })}
        search={true} onClick={onListClick} onError={onError} ref={refOSCheckList} />
      <Divider />
      {
        CPUTaskResult ?
          <>
            <CPUEvent title={intl.formatMessage({
              id: 'pages.diagnose.diagnosisresult',
              defaultMessage: 'Diagnosis result',
            })} subtitle={intl.formatMessage({
              id: 'pages.diagnose.eventsummary',
              defaultMessage: 'Event summary',
            })} data={CPUTaskResult} />
            <Divider />
            <CPUPieChart data={CPUTaskResult} />
            <Divider />
            <ProCard title={intl.formatMessage({
              id: 'pages.diagnose.dispatchingflamediagram',
              defaultMessage: 'Dispatching flame diagram',
            })} layout="center">
            <div style={{textAlign:'center', width:'100%'}}>
              <SmartIFrame src={svgUrl} />
              </div>
            </ProCard>
          </>
          :
          <></>
      }
    </PageContainer>
  );
};
