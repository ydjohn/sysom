// 面板Icon导入
import InputTextIcon from './assets/inputText.svg'
import InputDigitIcon from './assets/inputNumber.svg'
import SelectIcon from './assets/select.svg'
import FlameIcon from './assets/flame.svg'
import FlowIcon from './assets/flowchart.svg'
import MarkdownIcon from './assets/markdown.svg'
import LineChartIcon from './assets/linechart.svg'
import PieChartIcon from './assets/piechart.svg'
import TableIcon from './assets/table.svg'
import StatisticIcon from './assets/status.svg'
import RowIcon from './assets/menu.svg'

// 面板组件导入
import StatisticPannel from '../../components/StatisticPannel';
import PieChartPannel from '../../components/PieChartPannel';
import TablePannel from '../../components/TablePannel';
import TimeSeriesPannel from '../../components/TimeSeriesPannel';
import SvgPannel from '../../components/SvgPannel';
import FlowPannelPannel from '../../components/FlowPannel';
import MarkdownPannel from '../../components/MarkdownPannel';
import RowContainer from './components/sectionContainer/rowPanel'

export const componentConfigs = {
    'text': {
        type: 'text',
        name: '文本输入框',
        enName: 'text',
        container: 'taskform',
        icon: InputTextIcon,
        config: {
            key: `text${Date.now().toString()}`,
            type: 'text',
            name: 'text',
            label: 'text',
            initialValue: '',
            tooltip: '',
        },
        editForm: [
            {
                type: 'text',
                label: '输入框名称',
                width: 'xs',
                placeholder: '请输入名称',
                name: 'name'
            },
            {
                type: 'text',
                label: '输入框标签',
                width: 'xs',
                placeholder: '请输入标签',
                name: 'label'
            },
            {
                type: 'text',
                label: '输入框初始值',
                placeholder: '请输入初始值',
                width: 'md',
                name: 'initialValue',
            },
            {
                type: 'text',
                label: '输入框提示',
                placeholder: '请输入提示',
                width: 'md',
                name: 'tooltip'
            },
            {
                type: 'field',
                label: '子面板样式',
                name: 'style',
                children: [
                    {
                        type: 'digit',
                        label: '高度',
                        placeholder: '请输入高度',
                        width: 'sm',
                        name: 'height',
                        unit: 'px'
                    }
                ]
            },
        ]
    },
    'digit': {
        type: 'digit',
        name: '数字输入框',
        enName: 'digit',
        container: 'taskform',
        icon: InputDigitIcon,
        config: {
            key: `digit${Date.now().toString()}`,
            type: 'digit',
            name: 'digit',
            label: 'digit',
            initialValue: '',
            tooltip: '',
        },
        editForm: [
            {
                type: 'text',
                label: '输入框名称',
                width: 'xs',
                placeholder: '请输入名称',
                name: 'name'
            },
            {
                type: 'text',
                label: '输入框标签',
                width: 'xs',
                placeholder: '请输入标签',
                name: 'label'
            },
            {
                type: 'text',
                label: '输入框初始值',
                placeholder: '请输入初始值',
                width: 'md',
                name: 'initialValue',
            },
            {
                type: 'text',
                label: '输入框提示',
                placeholder: '请输入提示',
                width: 'md',
                name: 'tooltip'
            },
            {
                type: 'field',
                label: '子面板样式',
                name: 'style',
                children: [
                    {
                        type: 'digit',
                        label: '高度',
                        placeholder: '请输入高度',
                        width: 'sm',
                        name: 'height',
                        unit: 'px'
                    }
                ]
            }
        ]
    },
    'select': {
        type: 'select',
        name: '下拉选择框',
        enName: 'select',
        container: 'taskform',
        icon: SelectIcon,
        config: {
            key: `select${Date.now().toString()}`,
            type: 'select',
            name: 'select',
            label: 'select',
            initialValue: '',
            tooltip: '',
            options: [],
        },
        editForm: [
            {
                type: 'text',
                label: '输入框名称',
                width: 'xs',
                placeholder: '请输入名称',
                name: 'name'
            },
            {
                type: 'text',
                label: '输入框标签',
                width: 'xs',
                placeholder: '请输入标签',
                name: 'label'
            },
            {
                type: 'text',
                label: '输入框初始值',
                placeholder: '请输入初始值',
                width: 'md',
                name: 'initialValue',
            },
            {
                type: 'text',
                label: '输入框提示',
                placeholder: '请输入提示',
                width: 'md',
                name: 'tooltip'
            },
            {
                type: 'field',
                label: '子面板样式',
                name: 'style',
                children: [
                    {
                        type: 'digit',
                        label: '高度',
                        placeholder: '请输入高度',
                        width: 'sm',
                        name: 'height',
                        unit: 'px'
                    }
                ]
            },
            {
                type: 'list',
                label: '下拉选择框选项',
                name: 'options',
                form: [
                    {
                        type: 'text',
                        label: '选项标签',
                        placeholder: '请输入选项标签',
                        width: 'sm',
                        name: 'label'
                    },
                    {
                        type: 'text',
                        label: '选项值',
                        placeholder: '请输入选项值',
                        width: 'sm',
                        name: 'value'
                    },
                ]
            }
        ]
    },
    'stat': {
        type: 'stat',
        name: '统计图表',
        enName: 'statistic',
        container: 'panels',
        icon: StatisticIcon,
        config: {
            key: `stat${Date.now().toString()}`,
            type: 'stat',
            title: `统计图表 样例`,
            datasource: "stat_dataset",
            fieldConfig: {
                "mappings": [{
                    "type": "value",
                    "options": {
                        "正常": { "color": "blue" },
                        "危险": { "color": "red", },
                        "success": { "color": "green", "text": "成功" }
                    }
                }],
                "thresholds": {
                    "mode": "absolute",
                    "steps": [
                        { "color": "red", "value": 5 },
                        { "color": "green", "value": 0 }
                    ]
                },
                "unit": "%"
            },
        },
        editForm: [
            {
                type: 'text',
                label: '面板标题',
                width: 'xs',
                placeholder: '请输入名称',
                name: 'title'
            },
            {
                type: 'text',
                label: '数据源',
                width: 'xs',
                placeholder: '请输入数据源',
                name: 'datasource'
            },
            {
                type: 'field',
                label: '子面板样式',
                name: 'style',
                children: [
                    {
                        type: 'digit',
                        label: '高度',
                        placeholder: '请输入高度',
                        width: 'sm',
                        name: 'height',
                        unit: 'px'
                    }
                ]
            },
            {
                type: 'field',
                label: '选项参数',
                name: 'fieldConfig',
                children: [
                    {
                        type: 'list',
                        label: '映射',
                        name: 'mappings',
                        form: [
                            {
                                type: 'text',
                                label: '类型',
                                placeholder: '请输入类型',
                                width: 'sm',
                                name: 'type'
                            },
                            // {
                            //     type: 'key-list',
                            //     label: '映射项',
                            //     name: 'options',
                            //     key: {
                            //         type: 'text',
                            //         label: '映射项关键词',
                            //         placeholder: '请输入映射关键词',
                            //         width: 'sm',
                            //         name: 'key'
                            //     },
                            //     value: [
                            //         {
                            //             type: 'text',
                            //             label: '颜色',
                            //             placeholder: '请输入颜色',
                            //             width: 'sm',
                            //             name: 'color'
                            //         },
                            //         {
                            //             type: 'text',
                            //             label: '映射文本',
                            //             placeholder: '请输入文本',
                            //             width: 'sm',
                            //             name: 'text'
                            //         },
                            //     ]
                            // }
                        ]
                    },
                    {
                        type: 'field',
                        label: '阈值参数设置',
                        name: 'thresholds',
                        children: [
                            {
                                type: 'text',
                                label: '模式',
                                placeholder: '请输入模式',
                                width: 'sm',
                                name: 'mode'
                            },
                            {
                                type: 'list',
                                label: '阈值',
                                name: 'steps',
                                form: [
                                    {
                                        type: 'text',
                                        label: '颜色',
                                        placeholder: '请输入颜色',
                                        width: 'sm',
                                        name: 'color'
                                    },
                                    {
                                        type: 'text',
                                        label: '值',
                                        placeholder: '请输入值',
                                        width: 'sm',
                                        name: 'value'
                                    },
                                ]
                            }
                        ]
                    },
                    {
                        type: 'text',
                        label: '单位',
                        placeholder: '请输入单位',
                        width: 'sm',
                        name: 'unit'
                    }
                ]
            }
        ],
        component: StatisticPannel,
        datas: {
            "stat_dataset": {
                "data": [
                    { "key": "数据第一行（带color thresholds）", "value": 5.3 },
                    { "key": "数据第二行（带color mapping）", "value": "正常" },
                    { "key": "数据第三行（带color mapping）", "value": "危险" },
                    { "key": "数据第四行（带text mapping）", "value": "success" }
                ]
            }
        }
    },
    "piechart": {
        type: 'piechart',
        name: '饼图',
        enName: 'pie chart',
        container: 'panels',
        icon: PieChartIcon,
        config: {
            "key": "pieChartDemo",
            "type": "piechart",
            "title": "饼图 示意",
            "datasource": "pieChartDataSet"
        },
        editForm: [
            {
                type: 'text',
                label: '面板标题',
                width: 'xs',
                placeholder: '请输入名称',
                name: 'title'
            },
            {
                type: 'text',
                label: '数据源',
                width: 'xs',
                placeholder: '请输入数据源',
                name: 'datasource'
            },
            {
                type: 'field',
                label: '子面板样式',
                name: 'style',
                children: [
                    {
                        type: 'digit',
                        label: '高度',
                        placeholder: '请输入高度',
                        width: 'sm',
                        name: 'height',
                        unit: 'px'
                    }
                ]
            },
        ],
        component: PieChartPannel,
        datas: {
            "pieChartDataSet": {
                "data": [
                    { "key": "系列1", "value": 1 },
                    { "key": "系列2", "value": 2 },
                    { "key": "系列3", "value": 5 },
                    { "key": "系列4", "value": 2 }
                ]
            }
        }
    },
    "table": {
        type: 'table',
        name: '表格',
        enName: 'table',
        container: 'panels',
        icon: TableIcon,
        config: {
            "key": "TableDemo",
            "type": "table",
            "title": "表格DEMO",
            "datasource": "tableDataset",
            "fieldConfig": {
                "thresholds": {
                    "mode": "absolute",
                    "steps": [
                        { "color": "red", "value": 1000 },
                        { "color": "green", "value": 0 }
                    ]
                },
                "unit": "KB"
            }
        },
        editForm: [
            {
                type: 'text',
                label: '面板标题',
                width: 'xs',
                placeholder: '请输入名称',
                name: 'title'
            },
            {
                type: 'text',
                label: '数据源',
                width: 'xs',
                placeholder: '请输入数据源',
                name: 'datasource'
            },
            {
                type: 'field',
                label: '子面板样式',
                name: 'style',
                children: [
                    {
                        type: 'digit',
                        label: '高度',
                        placeholder: '请输入高度',
                        width: 'sm',
                        name: 'height',
                        unit: 'px'
                    }
                ]
            },
            {
                type: 'field',
                label: '选项参数',
                name: 'fieldConfig',
                children: [
                    {
                        type: 'field',
                        label: '阈值参数设置',
                        name: 'thresholds',
                        children: [
                            {
                                type: 'text',
                                label: '模式',
                                placeholder: '请输入模式',
                                width: 'sm',
                                name: 'mode'
                            },
                            {
                                type: 'list',
                                label: '阈值',
                                name: 'steps',
                                form: [
                                    {
                                        type: 'text',
                                        label: '颜色',
                                        placeholder: '请输入颜色',
                                        width: 'sm',
                                        name: 'color'
                                    },
                                    {
                                        type: 'text',
                                        label: '值',
                                        placeholder: '请输入值',
                                        width: 'sm',
                                        name: 'value'
                                    },
                                ]
                            }
                        ]
                    },
                    {
                        type: 'text',
                        label: '单位',
                        placeholder: '请输入单位',
                        width: 'sm',
                        name: 'unit'
                    }
                ]
            }
        ],
        component: TablePannel,
        datas: {
            "tableDataset": {
                "data": [
                    {
                        "key": 0, "第一列": "父第一行", "第二列": 200, "第三列": "300",
                        children: [
                            { "key": 1, "第一列": "子第一行", "第二列": "500", "第三列": "600" },
                            { "key": 2, "第一列": "子第二行", "第二列": "800", "第三列": "900" }
                        ]
                    },
                    { "key": 3, "第一列": "父第二行", "第二列": 1100, "第三列": "1120" }
                ]
            }
        }
    },
    "timeseries": {
        type: 'timeseries',
        name: '时序图面板',
        enName: 'time series',
        container: 'panels',
        icon: LineChartIcon,
        config: {
            "key": "timeseriesdemo",
            "type": "timeseries",
            "title": "时序图面板示例",
            "datasource": "TimeSeriesDataSet"
        },
        editForm: [
            {
                type: 'text',
                label: '面板标题',
                width: 'xs',
                placeholder: '请输入名称',
                name: 'title'
            },
            {
                type: 'text',
                label: '数据源',
                width: 'xs',
                placeholder: '请输入数据源',
                name: 'datasource'
            },
            {
                type: 'field',
                label: '子面板样式',
                name: 'style',
                children: [
                    {
                        type: 'digit',
                        label: '高度',
                        placeholder: '请输入高度',
                        width: 'sm',
                        name: 'height',
                        unit: 'px'
                    }
                ]
            }
        ],
        component: TimeSeriesPannel,
        datas: {
            "TimeSeriesDataSet": {
                "data": [
                    { "time": "2022-06-29 10:01:40", "webserver1": 33, "webserver2": 44, "webserver3": 20 },
                    { "time": "2022-06-29 10:02:40", "webserver1": 29, "webserver2": 50, "webserver3": 21 },
                    { "time": "2022-06-29 10:03:40", "webserver1": 10, "webserver2": 60, "webserver3": 22 },
                    { "time": "2022-06-29 10:04:40", "webserver1": 20, "webserver2": 55, "webserver3": 23 },
                    { "time": "2022-06-29 10:05:40", "webserver1": 33, "webserver2": 30, "webserver3": 30 },
                    { "time": "2022-06-29 10:06:40", "webserver1": 35, "webserver2": 20, "webserver3": 31 },
                    { "time": "2022-06-29 10:07:40", "webserver1": 40, "webserver2": 10, "webserver3": 40 },
                    { "time": "2022-06-29 10:08:40", "webserver1": 80, "webserver2": 35, "webserver3": 41 },
                    { "time": "2022-06-29 10:09:40", "webserver1": 60, "webserver2": 60, "webserver3": 50 },
                    { "time": "2022-06-29 10:10:40", "webserver1": 67, "webserver2": 70, "webserver3": 60 },
                    { "time": "2022-06-29 10:11:40", "webserver1": 50, "webserver2": 65, "webserver3": 30 },
                    { "time": "2022-06-29 10:12:40", "webserver1": 40, "webserver2": 44, "webserver3": 44 },
                    { "time": "2022-06-29 10:13:40", "webserver1": 30, "webserver2": 33, "webserver3": 20 },
                    { "time": "2022-06-29 10:14:40", "webserver1": 20, "webserver2": 10, "webserver3": 10 },
                    { "time": "2022-06-29 10:15:40", "webserver1": 33, "webserver2": 44, "webserver3": 20 },
                    { "time": "2022-06-29 10:16:40", "webserver1": 29, "webserver2": 50, "webserver3": 21 },
                    { "time": "2022-06-29 10:17:40", "webserver1": 10, "webserver2": 60, "webserver3": 22 },
                    { "time": "2022-06-29 10:18:40", "webserver1": 20, "webserver2": 55, "webserver3": 23 },
                    { "time": "2022-06-29 10:19:40", "webserver1": 33, "webserver2": 30, "webserver3": 30 },
                    { "time": "2022-06-29 10:20:40", "webserver1": 35, "webserver2": 20, "webserver3": 31 },
                    { "time": "2022-06-29 10:21:40", "webserver1": 40, "webserver2": 10, "webserver3": 40 },
                    { "time": "2022-06-29 10:22:40", "webserver1": 80, "webserver2": 35, "webserver3": 41 },
                    { "time": "2022-06-29 10:23:40", "webserver1": 60, "webserver2": 60, "webserver3": 50 },
                    { "time": "2022-06-29 10:24:40", "webserver1": 67, "webserver2": 70, "webserver3": 60 },
                    { "time": "2022-06-29 10:25:40", "webserver1": 50, "webserver2": 65, "webserver3": 30 },
                    { "time": "2022-06-29 10:26:40", "webserver1": 40, "webserver2": 44, "webserver3": 44 },
                    { "time": "2022-06-29 10:27:40", "webserver1": 30, "webserver2": 33, "webserver3": 20 },
                    { "time": "2022-06-29 10:28:40", "webserver1": 20, "webserver2": 10, "webserver3": 10 }
                ]
            },
        }
    },
    "svg": {
        type: 'svg',
        name: 'SVG',
        enName: 'svg',
        container: 'panels',
        icon: FlameIcon,
        config: {
            "key": "火焰图",
            "type": "svg",
            "title": "调度火焰图",
            "datasource": "svgdata"
        },
        editForm: [
            {
                type: 'text',
                label: '面板标题',
                width: 'xs',
                placeholder: '请输入名称',
                name: 'title'
            },
            {
                type: 'text',
                label: '数据源',
                width: 'xs',
                placeholder: '请输入数据源',
                name: 'datasource'
            },
            {
                type: 'field',
                label: '子面板样式',
                name: 'style',
                children: [
                    {
                        type: 'digit',
                        label: '高度',
                        placeholder: '请输入高度',
                        width: 'sm',
                        name: 'height',
                        unit: 'px'
                    }
                ]
            }
        ],
        component: SvgPannel,
        datas: {
            "svgdata": {
                "data": [
                    { "key": 0, "value": "<这边放火焰图svg文本>" },
                ]
            }
        }
    },
    "flow": {
        type: 'flow',
        name: '流程图',
        enName: 'flow chart',
        container: 'panels',
        icon: FlowIcon,
        config: {
            "key": "flowchart",
            "type": "flow",
            "title": "flow测试",
            "flowconfigs": {
                nodes: [
                    { id: '内核驻留', x: 40, y: 40, },
                    { id: '位置展示', x: 40, y: 200, },
                ],
                edges: [
                    { source: '内核驻留', target: { cell: '位置展示', port: 'top' } },
                ]
            },
            "datasource": "dataFlow",
            "links": {
                defalut: {
                    type: "popup",
                    name: "popup",
                    pannel: {
                        "key": "procPopup",
                        "type": "timeseries",
                        "title": "${key}的详细数据",
                        "datasource": "dataTimeSeries"
                    }
                }
            }
        },
        editForm: [
            {
                type: 'text',
                label: '面板标题',
                width: 'xs',
                placeholder: '请输入名称',
                name: 'title'
            },
            {
                type: 'field',
                label: '子面板样式',
                name: 'style',
                children: [
                    {
                        type: 'digit',
                        label: '高度',
                        placeholder: '请输入高度',
                        width: 'sm',
                        name: 'height',
                        unit: 'px'
                    }
                ]
            },
            {
                type: 'field',
                label: '流程图配置',
                name: 'flowconfigs',
                children: [
                    {
                        type: 'list',
                        label: '节点',
                        name: 'nodes',
                        form: [
                            {
                                type: 'text',
                                label: '节点ID',
                                placeholder: '请输入节点ID',
                                width: 'sm',
                                name: 'id'
                            },
                            {
                                type: 'text',
                                label: '节点X坐标',
                                placeholder: '请输入节点X坐标',
                                width: 'sm',
                                name: 'x'
                            },
                            {
                                type: 'text',
                                label: '节点Y坐标',
                                placeholder: '请输入节点Y坐标',
                                width: 'sm',
                                name: 'y'
                            },
                        ]
                    },
                    {
                        type: 'list',
                        label: '边',
                        name: 'edges',
                        form: [
                            {
                                type: 'text',
                                label: '源节点',
                                placeholder: '请输入源节点',
                                width: 'sm',
                                name: 'source'
                            },
                            {
                                type: 'text',
                                label: '目标节点',
                                placeholder: '请输入目标节点',
                                width: 'sm',
                                name: 'target'
                            },
                        ]
                    }
                ]
            },
            {
                type: 'text',
                label: '数据源',
                width: 'xs',
                placeholder: '请输入数据源',
                name: 'datasource'
            },
            {
                type: 'field',
                label: '链接配置',
                name: 'links',
                children: [
                    {
                        type: 'field',
                        label: '默认链接',
                        name: 'defalut',
                        children: [
                            {
                                type: 'text',
                                label: '链接类型',
                                placeholder: '请输入链接类型',
                                width: 'sm',
                                name: 'type'
                            },
                            {
                                type: 'text',
                                label: '链接名称',
                                placeholder: '请输入链接名称',
                                width: 'sm',
                                name: 'name'
                            },
                            {
                                type: 'field',
                                label: '链接面板配置',
                                name: 'pannel',
                                children: [
                                    {
                                        type: 'text',
                                        label: '面板标题',
                                        placeholder: '请输入面板标题',
                                        width: 'sm',
                                        name: 'title'
                                    },
                                    {
                                        type: 'text',
                                        label: '数据源',
                                        placeholder: '请输入数据源',
                                        width: 'sm',
                                        name: 'datasource'
                                    },
                                ]
                            }
                        ]
                    }
                ]
            }
        ],
        component: FlowPannelPannel,
        datas: {
            "dataFlow": {
                "data": [
                    { "key": "内核驻留", "title": "内核驻留", "text": "Max:350 AVA:300 Min:100" },
                    { "key": "位置展示", "title": "Title位置", "value": "value位置", "text": "text位置" },
                ]
            }
        }
    },
    "markdown": {
        type: 'markdown',
        name: 'Markdown',
        enName: 'markdown',
        container: 'panels',
        icon: MarkdownIcon,
        config: {
            "key": "rca_result",
            "type": "markdown",
            "title": "",
            "datasource": "RcaResult"
        },
        editForm: [
            {
                type: 'text',
                label: '面板标题',
                width: 'xs',
                placeholder: '请输入名称',
                name: 'title'
            },
            {
                type: 'text',
                label: '数据源',
                width: 'xs',
                placeholder: '请输入数据源',
                name: 'datasource'
            },
            {
                type: 'field',
                label: '子面板样式',
                name: 'style',
                children: [
                    {
                        type: 'digit',
                        label: '高度',
                        placeholder: '请输入高度',
                        width: 'sm',
                        name: 'height',
                        unit: 'px'
                    }
                ]
            }
        ],
        component: MarkdownPannel,
        datas: {
            "RcaResult": {
                "data": "# 这是一级标题\n## 这是二级标题"
            }
        }
    },
    "row": {
        type: 'row',
        name: '布局面板',
        enName: 'row',
        container: 'panels',
        icon: RowIcon,
        component: RowContainer,
        config: {
            "key": "pieRow",
            "type": "row",
            "title": "测试行",
            "datasource": "",
            "style": {
                "height": "unset"
            },
            "children": [
                {
                    "key": "pieChartDemo",
                    "type": "piechart",
                    "title": "标题对应title字段",
                    "datasource": "pieChartDataSet"
                },
                {
                    "key": "pieChartDemo2",
                    "type": "piechart",
                    "title": "内核内存",
                    "datasource": "pieChartDataSet"
                },
                {
                    "key": "pieChartDemo3",
                    "type": "piechart",
                    "title": "用户态内存",
                    "datasource": "pieChartDataSet"
                },
            ]
        },
        editForm: [
            {
                type: 'text',
                label: '面板标题',
                width: 'xs',
                placeholder: '请输入名称',
                name: 'title'
            },
            {
                type: 'field',
                label: '子面板样式',
                name: 'style',
                children: [
                    {
                        type: 'digit',
                        label: '高度',
                        placeholder: '请输入高度',
                        width: 'sm',
                        name: 'height',
                        unit: 'px'
                    }
                ]
            },
            {
                type: 'list',
                label: '子面板',
                name: 'children',
                add: false,
                max: 3,
                form: [
                    {
                        type: 'text',
                        label: '子面板标题',
                        placeholder: '请输入子面板标题',
                        width: 'md',
                        name: 'title'
                    },
                    {
                        type: 'text',
                        label: '子面板数据源',
                        placeholder: '请输入子面板数据源',
                        width: 'md',
                        name: 'datasource'
                    },
                ]
            }
        ],
    }
}
