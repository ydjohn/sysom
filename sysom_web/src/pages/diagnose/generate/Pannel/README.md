# Panel Config Generator Manual
## Structure introduction
```
├── assets                             # Static resource file
├── component-config.js                # Component configs (IMPORTANT)
├── components
│   ├── componentContainer             # render components in preview container
│   │   ├── index.jsx
│   │   └── index.less
│   ├── componentToolbar               # component tools (drag/edit/delete)
│   │   ├── index.jsx
│   │   ├── index.less
│   │   └── renderForm.jsx             # json to form renderer
│   ├── createComponent                # determined which component to render
│   │   └── index.jsx
│   ├── dragPanel                      # make component to be draggable
│   │   ├── index.jsx
│   │   └── index.less
│   ├── previewContainer               # the preview area
│   │   ├── index.jsx
│   │   └── index.less
│   └── sectionContainer               # divide preview area into two sections (taskform, panels)
│       ├── index.jsx
│       └── index.less
├── constants                          # constant variables
│   └── item.js
├── index.jsx                          # main entry
├── index.less
└── util.js                            # util functions
```

## How to add more components?
You can add more components by editing `component-config,js`, which includes one js object called `componentConfigs`. Add Elements into it using type as its name and follow the format shown in the table below.
### Config Parameters
| parameter | type        | required | notes                                                     |
| --------- | ----------- | -------- | --------------------------------------------------------- |
| type      | string      | true     | have to be unique                                         |
| name      | string      | true     | will be display as a label                                |
| enName    | string      |          | english name                                              |
| container | string      | true     | container that can be dropped                             |
| icon      | image file  | true     | icon of the component, will be displayed in the dragPanel |
| gridSpan  | number(1-3) | true     | How much space does it take up                            |
| config    | Object      | true     | config props to be ejected                                |
| editForm  | Object      |          | Define the form for editing this component                |
#### editForm Parameters
##### Text
| parameter   | type   | required | notes                                              |
| ----------- | ------ | -------- | -------------------------------------------------- |
| type        | string | true     | field type                                         |
| label       | string | true     | field label                                        |
| width       | string | true     | field width                                        |
| placeholder | string | true     |                                                    |
| name        | string | true     | Must correspond one-to-one with the keys in config |

##### List
| parameter | type   | required | notes                                              |
| --------- | ------ | -------- | -------------------------------------------------- |
| type      | string | true     | field type                                         |
| label     | string | true     | field label                                        |
| name      | string | true     | Must correspond one-to-one with the keys in config |
| form      | array  | true     | form in the list                                   |
refer to [ProFormList](https://procomponents.ant.design/components/group)

##### Field
to deal with config parameters which is nested in another parameter
```json
{
    "example": {
        "param one": {},
        "param two": {}
    }
}
```
you can define it like this
```json
{
    "type": "field",
    "label": "example",
    "name": "example",
    "children": [
        {
            "type": "text",
            "label": "param one",
            "name": "param one",
            "placeholder": "param one",
            "width": "sm"
        },
        {
            "type": "text",
            "label": "param two",
            "name": "param two",
            "placeholder": "param two",
            "width": "sm"
        }
    ]
}
```
| parameter | type   | required | notes                                              |
| --------- | ------ | -------- | -------------------------------------------------- |
| type      | string | true     | field type                                         |
| label     | string | true     | field label                                        |
| name      | string | true     | Must correspond one-to-one with the keys in config |
| children  | array  | true     | children nested in the field                       |