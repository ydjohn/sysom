/*
 * @Author: wb-msm241621
 * @Date: 2024-03-05 17:12:08
 * @LastEditTime: 2024-03-11 16:01:03
 * @Description: 
 */
import { PageContainer } from '@ant-design/pro-layout';
import _ from 'lodash';
import { request, FormattedMessage } from 'umi';
import { Divider, Modal } from 'antd';
import QueryForm from './components/QueryForm';
import TaskList from '../components/TaskList';
import Dashboard from '../components/Dashboard';
import { useEffect, useState, useRef } from 'react';
import { getTask } from '../service';


/**
 * @Description: 为元组插入元素到指定索引
 * @param {*} tuple 指定元组
 * @param {*} index 指定索引
 * @param {*} value 需要插入的元素
 * @return {*} newTuple 插入新元素后的元组
 */
function insertIntoTuple(tuple, index, value) {
    if (index >= 0 && index <= tuple.length) {
        let newTuple = tuple.slice();
        newTuple.splice(index, 0, value);
        return newTuple;
    } else {
        throw new Error("Error: index exceeds array range");
    }
}


const DiagnoseQuery = () => {
    const resultListRef = useRef([]);
    const [multichannelConfig, setMultichannelConfig] = useState([
        {
            "name": "ssh",
            "label": "SSH通道",
            "extra_params": {
                "*": []
            }
        }
    ]);
    const initialierQueryParams = {
        task_id: "",
        channel: "",
        params: [{ "key": "instance", "value": "" }]
    }
    const [data, setData] = useState();
    const [params, setParams] = useState(initialierQueryParams);
    const [diagnoseMenus, setDiagnoseMenus] = useState([]);
    const [pannelConfig, setPannelConfig] = useState();
    const chanelEnum = new Object()
    const [serviceNameEnum, setServiceNameEnum] = useState({})

    useEffect(() => {
        request("/resource/diagnose/v2/multichannel.json")
            .catch(err => {
                return new Promise.resolve({
                    "version": 1.0,
                    "channels": [
                        {
                            "name": "ssh",
                            "label": "SSH通道",
                            "extra_params": {
                                "*": []
                            }
                        }
                    ]
                })
            })
            .then(res => {
                setMultichannelConfig(res["channels"])
                return request("/resource/diagnose/v2/locales.json")
            })
            .then(res => {
                const { menus } = res;
                const snMenu = new Object({})
                menus.map(item => {
                    const itemTuple = _.split(item, '.')
                    const serviceName = itemTuple[itemTuple.length - 1]
                    snMenu[serviceName] = { text: <FormattedMessage id={item} defaultMessage={serviceName} /> }
                })
                setServiceNameEnum(snMenu)
                const diagnosePaths = menus.map(item => {
                    const s = _.replace(item, "menu.", "")
                    const new_s = insertIntoTuple((_.split(s, ".")), 1, "v2")
                    return `/resource/${_.join(new_s, "/")}.json`
                })
                setDiagnoseMenus(diagnosePaths);
            })
    }, [])

    multichannelConfig.map((value) => {
        chanelEnum[value.name] = { text: value.label }
    })

    const onError = async (record) => {
        // window.open(`/diagnose/detail/${record.task_id}`)
        const msg = await getTask(record.task_id);
        Modal.error({
            title: '诊断失败',
            content: (
                <div>
                    <div>错误信息: {msg?.err_msg ? msg.err_msg : msg?.result}</div>
                </div>
            ),
        });
    }

    const refreshTask = async (record) => {
        const msg = await getTask(record.task_id);
        const index = _.findLastIndex(diagnoseMenus, (item) => { return item.includes(msg.service_name); });
        request(diagnoseMenus[index])
            .then(res => {
                setPannelConfig(res);
                setData({ ...msg, ...msg.result });
            })
    }

    return (
        <PageContainer>

            {/* 查询参数搜索表单 */}
            <QueryForm
                params={initialierQueryParams}
                chanelEnum={chanelEnum}
                serviceNameEnum={serviceNameEnum}
                onDone={(v) => { setParams(v); resultListRef.current.reload(); }}
                onReset={() => { setParams(initialierQueryParams); resultListRef.current.reload(); }}
            />
            <Divider />
            {/* 查询列表 */}
            <TaskList params={params} onClick={refreshTask} onError={onError} ref={resultListRef} />
            <Divider />
            {/* 诊断成功结果展示 */}
            {
                data && <Dashboard
                    variables={pannelConfig.variables}
                    serviceName={pannelConfig.servername}
                    pannels={pannelConfig.pannels}
                    datas={data}
                    refreshTask={refreshTask}
                />

            }
        </PageContainer>
    )
}

export default DiagnoseQuery;
