import { Layout, Carousel, Menu } from 'antd';
import { useIntl, FormattedMessage } from 'umi';
// import AvatarDropdown from '@/components/RightContent/AvatarDropdown';
import styles from './intro.less';
import Footer from '@/components/Footer';
import React from 'react';

const { SubMenu } = Menu;
const { Header, Content } = Layout;

const imgBanner = [
    { key: 1, banner: "none", title: "龙蜥运维SIG", urls: 'https://openanolis.cn/sig/sysom', des: "" },
    { key: 2, banner: "none", title: "龙蜥社区系统运维联盟", urls: 'https://soma.openanolis.cn/', des: "" },
    { key: 4, banner: "none", title: "SysOM开源Demo", urls: 'http://sysom.openanolis.cn/', des: "" },
]

const IntroComponent = (props) => {
    return (
        <Carousel autoplay>
            {
                imgBanner.map(item => (
                    <div className={styles.banner} key={item.key}>
                        <div className={styles.title}>
                            <h3>{item.title}</h3>
                            <a href={item.urls}><FormattedMessage id="pages.click.enter" /></a>
                        </div>
                        {/* <div className={styles.img} style={{ backgroundColor: item.banner }}></div> */}
                    </div>
                ))
            }
        </Carousel>
    );
}

export default IntroComponent;