/// <reference types="cypress" />

describe("SysOM Cluster Manager Test", () => {
    beforeEach(() => {
        cy.login()
    })
    it("Create and delete cluster", () => {
        // 
        cy.intercept("POST", "/api/v1/cluster/")
            .as("createCluster")

        cy.intercept("DELETE", "/api/v1/cluster/*")
            .as("deleteCluster")

        // 1. 访问主机列表
        cy.visit("/host/cluster")

        // 2. 点击新建集群打开模块框
        cy.get("button").contains("新建集群").click()

        // 3. 在模态框内部填充字段
        cy.get(".ant-modal-content").first().within(() => {

            // 3.1 cluster name
            cy.get("#cluster_name").focus().type("test_cluster")

            // 3.2 cluster description
            cy.get("#cluster_description").focus().clear().type("Test description")

            // 3.3 点击确认触发新建集群请求
            cy.get("button").contains("确 认").click()

            // 3.4 等待新建集群请求结束，判断请求是否成功
            // 检查状态码返回是否是200（如果集群已经存在会返回400）
            cy.wait('@createCluster').its("response.statusCode").should("eq", 200)
        })


        // 创建完集群后等待一秒钟，一秒钟后执行删除操作
        cy.wait(1000)

        // 找到最新创建的集群，并点击删除按钮
        cy.get("td")
            .contains("test_cluster")
            .parent()
            .within(() => {
                cy.get("td").contains("删除").click()
            })

        // 点击删除按钮之后需要在弹出的浮窗中点击OK确认
        cy.get(".ant-popover-buttons").find("button").contains("OK").click()

        // 确认删除接口调用结果为 200
        cy.wait('@deleteCluster')
            .then((interception) => {
                cy.wrap({
                    statusCode: interception.response?.statusCode
                }).its("statusCode").should("eq", 200)
            })

    })
})