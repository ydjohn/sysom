#!/bin/bash
BaseDir=$(dirname $(readlink -f "$0"))
SERVICE_NAME=sysom-hotfix

clear_app() {
    
    sed -i '/hotfix/d' /etc/exports
    exportfs -rv

    rm -rf /etc/supervisord.d/${SERVICE_NAME}.ini
    # remove the hotfix_builder nfs share base directory
    rm -rf ${SERVER_HOME}/hotfix_builder

    systemctl stop nfs-server
    ###use supervisorctl update to stop and clear services###
    supervisorctl update
}

# Stop first
bash -x $BaseDir/stop.sh

clear_app
