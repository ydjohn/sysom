#!/bin/bash
SERVICE_SCRIPT_DIR=$(basename $(dirname $0))
SERVICE_HOME=${MICROSERVICE_HOME}/${SERVICE_SCRIPT_DIR}
SERVICE_SCRIPT_HOME=${MICROSERVICE_SCRIPT_HOME}/${SERVICE_SCRIPT_DIR}
VIRTUALENV_HOME=$GLOBAL_VIRTUALENV_HOME
SERVICE_NAME=sysom-ad_proxy

if [ "$UID" -ne 0 ]; then
    echo "Please run as root"
    exit 1
fi

install_requirement() {
    pushd ${SERVICE_SCRIPT_HOME}
    pip install -r requirements.txt
    popd
}

source_virtualenv() {
    echo "INFO: activate virtualenv..."
    source ${VIRTUALENV_HOME}/bin/activate || exit 1
}

install_app() {
    source_virtualenv
    install_requirement
}

install_app
