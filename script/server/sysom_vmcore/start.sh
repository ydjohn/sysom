#!/bin/bash
SERVICE_NAME=sysom-vmcore

is_start() {
    status=`supervisorctl status ${SERVICE_NAME} | awk '{print $2}'`
    result=`echo "RUNNING STARTING" | grep $status`
    if [[ "$result" != "" ]]
    then 
        return 1
    else
        return 0
    fi 
}

start_app() {
    is_start
    if [[ $? == 0 ]];then
        systemctl start nfs-server
        systemctl start rpcbind
        systemctl start nfs
        sed -i '/vmcore/s;^#;;g' /var/spool/cron/root
        sed -i '/vmcore/s;^#;;g' /etc/exports
        exportfs -rv
        supervisorctl start $SERVICE_NAME
        is_start
        if [[ $? == 0 ]]; then
            echo "${SERVICE_NAME} service start fail, please check log" 
            exit 1
        else
            echo "supervisorctl start ${SERVICE_NAME} success..."
        fi
    fi
}

start_app
