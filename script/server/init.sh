#!/bin/bash -x
BaseDir=$(dirname $(readlink -f "$0"))
FIRST_INIT_DONE=0

cur_script_home=$(dirname $BaseDir)
pushd ${cur_script_home}
if [ $FIRST_INIT_DONE == 0 ]; then
    bash +x sysom.sh install ALL
    bash +x sysom.sh init ALL

    local_app_home=$(dirname ${cur_script_home})
    if [ "${local_app_home}" == "${APP_HOME}" ] || [ -z "$APP_HOME" ] || [ -z "$SCRIPT_HOME" ]; then
        sed -i 's/^FIRST_INIT_DONE=0/FIRST_INIT_DONE=1/g' $0
    else
        sed -i 's/^FIRST_INIT_DONE=0/FIRST_INIT_DONE=1/g' ${SCRIPT_HOME}/server/init.sh
    fi
else
    bash +x sysom.sh start ALL
fi
popd
