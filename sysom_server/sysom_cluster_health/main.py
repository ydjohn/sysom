# -*- coding: utf-8 -*- #
from conf.settings import *
from os import getpid
from multiprocessing import Queue
from clogger import logger
from fastapi import FastAPI
from conf.settings import YAML_CONFIG
from sysom_utils import CmgPlugin, SysomFramework
from app.collector.metric_manager import MetricManager
from app.routers import health, metrics
from app.collector.collector import Collector
from app.health_calculator.calculator import HealthCalculator
from app.diagnose.diagnose_worker import DiagnoseWorker
from app.consumer.consumer import HealthMetricListener
from app.crud import del_all_abnormal_metrics_data
from app.database import SessionLocal

app = FastAPI()

app.include_router(health.router, prefix="/api/v1/cluster_health/health")
app.include_router(metrics.router)

#############################################################################
# Write your API interface here, or add to app/routes
#############################################################################


def init_framwork():
    SysomFramework\
        .init(YAML_CONFIG) \
        .load_plugin_cls(CmgPlugin) \
        .start()
    logger.info("SysomFramework init finished!")
    
    
def cleanup_gcache_data():
    gcache_names = [
        CLUSTER_HEALTH_METRIC_GCACHE,
        NODE_HEALTH_METRIC_GCACHE,
        POD_HEALTH_METRIC_GCACHE,
        CLUSTER_METRIC_EXPORTER,
        NODE_METRIC_EXPORTER,
        POD_METRIC_EXPORTER
    ]

    for name in gcache_names:
        gcache = SysomFramework.gcache(name)
        gcache.clean()
        
    if ABNORMAL_METRIC_STORAGE == "mysql":
        with SessionLocal() as db:
            del_all_abnormal_metrics_data(db)

@app.on_event("startup")
async def on_start():
    init_framwork()
    
    cleanup_gcache_data()
    
    # load all registered metrics from settings
    metric_manager = MetricManager()
    metric_manager.metric_register()

    diagnose_queue = Queue(maxsize=MAX_QUEUE_SIZE)
    pid = getpid();

    # start analyzer to collect and calculate health score
    try:
        Collector(
            queue=diagnose_queue,
            metric_manager=metric_manager,
            parent_pid=pid
        ).start()
        
        HealthCalculator(
            parent_pid=pid
        ).start()

        DiagnoseWorker(
            metric_manager=metric_manager,
            queue=diagnose_queue,
            parent_pid=pid).start()
        
        HealthMetricListener().start()
        logger.info("集群健康度定时任务已启动")
    except Exception as e:
        logger.exception(e)

    ##########################################################################
    # Perform some microservice initialization operations over here
    ##########################################################################


@app.on_event("shutdown")
async def on_shutdown():
    cleanup_gcache_data()
