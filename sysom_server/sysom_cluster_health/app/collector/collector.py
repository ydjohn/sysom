import time
from conf.settings import *
from multiprocessing import Queue, Process
from schedule import Scheduler
from os import getpid, kill
from typing import Dict
from clogger import logger
from app.collector.metric_manager import MetricManager
from app.collector.metric_exception import MetricProcessException
from lib.common_type import Labels, Level
from lib.utils import collect_all_clusters, collect_instances_of_cluster, \
    collect_pods_of_instance


class Collector(Process):
    def __init__(
        self,
        queue: Queue = None,
        metric_manager: MetricManager = None,
        parent_pid: int = None
    ) -> None:
        super().__init__(daemon=True)
        self.collect_interval = COLLECT_INTERVAL
        self.collector_host_schedule: Scheduler = Scheduler()
        self.metric_manager = metric_manager
        self.last_end_time = time.time() - self.collect_interval
        self.last_alarm_table: Dict[str, int] = {}
        self.queue = queue
        self.parent_pid = parent_pid
    
    def _check_if_parent_is_alive(self):
        try:
            kill(self.parent_pid, 0)
        except OSError:
            logger.info(f"Analyzer's parent {self.parent_pid} is exit")
            exit(0)
            
    def _deliver_one_alarm(self, metric, labels: Labels, level: Level,
                          score: float, value: float):
        if metric.settings.alarm is None:
            return

        type = metric.settings.type
        threshold = metric.settings.alarm.threshold
        metric_id = metric.settings.metric_id

        key = f"{labels.cluster}-{labels.instance}-{labels.namespace}" \
            f"-{labels.pod}-{metric_id}"

        # score lower than threshold, deliver alarm
        if score <= threshold:
            if key not in self.last_alarm_table:
                self.last_alarm_table[key] = 0

            continue_alarm = self.last_alarm_table[key]
            # first alarm, deliver it and raise diagnose
            if continue_alarm == 0:
                alart_id = metric.deliver_alarm(value, type)
                metric.deliver_diagnose(alart_id, level, type, self.queue)

            self.last_alarm_table[key] += 1

            # if alarm list is longer than MERGE_NUM, resend the alarms
            if continue_alarm > ALARM_MERGE_NUM:
                self.last_alarm_table[key] = 0
        else:
            # if continuesly alarm end, reset the alarm list
            if key in self.last_alarm_table:
                del self.last_alarm_table[key]

    def _collect_process_one(self, level: Level, labels: Labels):
        """Collect and process one cluster/node/pod's all metrics

        Args:
            level (Level): cluster/node/pod
            labels (Labels): cluster/node/pod labels
        """
        
        for metric in self.metric_manager.registed_metric[level]:
            try:
                value, score = metric.metric_score(labels, self.last_end_time)
            except MetricProcessException as e:
                logger.info(f"Calculate Metric: {metric.settings.metric_id} "
                            f"of Pod: {labels.pod} of Node: {labels.instance} "
                            f"of Cluster: {labels.cluster} failed {e}")
                continue
            
            metric.deliver_health_metric(value, score)
            self._deliver_one_alarm(metric, labels, level, score, value)

    def _collect_process_pod_metric(self, name:str, ns:str,
                                    level: Level, labels: Labels):
        pod_label = Labels(
            cluster=labels.cluster,
            instance=labels.instance,
            pod=name,
            namespace=ns
        )

        # collect and process a pod's all metrics
        self._collect_process_one(level, pod_label)
    
    def _collect_process_node_metric(self, name:str, level: Level, 
                                     labels: Labels):
        pods_list = []
        node_label = Labels(cluster=labels.cluster, instance=name)
        
        # collect and process a node's all metrics
        self._collect_process_one(level, node_label)
        
        pods_list = collect_pods_of_instance(
            name,
            self.metric_manager.metric_reader,
            self.collect_interval
        )
        
        for pod, ns in pods_list:
            self._collect_process_pod_metric(pod, ns, Level.Pod, node_label)
            
            
    def _collect_process_cluster_metric(self, cluster: str):
        level = Level.Cluster
        labels = Labels(cluster=cluster)
        nodes_list = []

        # collect and process a cluster's all metrics
        self._collect_process_one(level, labels)
        
        nodes_list = collect_instances_of_cluster(
            cluster,
            self.metric_manager.metric_reader,
            self.collect_interval
        )
        
        for node in nodes_list:
            self._collect_process_node_metric(node, Level.Node, labels)
            
                
    def _register_task(self):
        cluster_list = []
        
        cluster_list = collect_all_clusters(self.metric_manager.metric_reader)
        # no cluster label, we assume just one, and names it "dafault"
        if len(cluster_list) == 0 or NO_CLUSTER_LABEL is True:
            cluster_list.append("default")

        start_time = time.time()

        for cluster in cluster_list:
            self._collect_process_cluster_metric(cluster)

        end_time = time.time()
        self.last_end_time = end_time
        logger.info(f"Excutaion time: {end_time - start_time}")
    

    def run(self) -> None:
        logger.info(f'健康度内置指标采集分析守护进程PID： {getpid()}')

        self._register_task()
        self.collector_host_schedule.every(self.collect_interval)\
            .seconds.do(self._register_task)

        while True:
            self._check_if_parent_is_alive();
            
            if self.is_alive():
                self.collector_host_schedule.run_pending()
            else:
                break
            time.sleep(max(1, int(self.collect_interval / 2)))
    
