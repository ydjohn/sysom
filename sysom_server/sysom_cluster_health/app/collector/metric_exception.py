class MetricException(Exception):
    """Metric base exception

    This class defines the base exception for Cluster Health Calculator,
    and all exceptions thrown during health calculator should inherit 
    from this class.
    """


class MetricSettingsException(MetricException):
    """Exceptions thrown for Metric settings

    This exception should be thrown if the Metric has invalid settings.
    """


class MetricCollectException(MetricException):
    """Exceptions thrown for Metric collection

    This exception should be thrown if the Metric collection fails.
    """


class MetricProcessException(MetricException):
    """Exceptions thrown for Metric process

    This exception should be thrown if the Metric process fails.
    """
