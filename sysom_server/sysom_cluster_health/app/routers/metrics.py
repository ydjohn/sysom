import json
from conf.settings import *
from prometheus_client import Gauge, CollectorRegistry, generate_latest
from fastapi import APIRouter, HTTPException, Response
from app.health_calculator.score_result import ScoreType
from sysom_utils import SysomFramework
from clogger import logger

CLUSTER_HEALTH_SCORE_LABEL = ["cluster", "type"]
CLUSTER_HEALTH_METRIC_LABEL = ["cluster", "type", "description", "mode"]
NODE_HEALTH_SCORE_LABEL = ["cluster", "instance", "type"]
NODE_HEALTH_METRIC_LABEL = [
    "cluster",
    "instance",
    "type",
    "description",
    "mode"]
POD_HEALTH_SCORE_LABEL = ["cluster", "instance", "pod", "namespace", "type"]
POD_HEALTH_METRIC_LABEL = [
    "cluster",
    "instance",
    "type",
    "pod",
    "namespace",
    "description",
    "mode"]


registry = CollectorRegistry()
cluster_health_score = Gauge('sysom_cluster_health_score',
                             'sysom cluster health score',
                             CLUSTER_HEALTH_SCORE_LABEL,
                             registry=registry)
cluster_health_metric = Gauge('sysom_cluster_health_metric',
                              'sysom cluster health metric',
                              CLUSTER_HEALTH_METRIC_LABEL,
                              registry=registry)
node_health_score = Gauge('sysom_node_health_score',
                          'sysom node health score',
                          NODE_HEALTH_SCORE_LABEL,
                          registry=registry)
node_health_metric = Gauge('sysom_node_health_metric',
                           'sysom node health metric',
                           NODE_HEALTH_METRIC_LABEL,
                           registry=registry)
pod_health_score = Gauge('sysom_pod_health_score',
                         'sysom pod health score',
                         POD_HEALTH_SCORE_LABEL,
                         registry=registry)
pod_health_metric = Gauge('sysom_pod_health_metric',
                          'sysom pod health score',
                          POD_HEALTH_METRIC_LABEL,
                          registry=registry)


router = APIRouter()

@router.get("/metrics")
def get_metrics():
    # pull health score metric from redis and push to prometheus
    g_cache_cluster = SysomFramework.gcache(CLUSTER_METRIC_EXPORTER)
    g_cache_instance = SysomFramework.gcache(NODE_METRIC_EXPORTER)
    g_cache_pod = SysomFramework.gcache(POD_METRIC_EXPORTER)
    
    try:
        cluster_all = g_cache_cluster.load_all()
        nodes_all = g_cache_instance.load_all()
        pods_all = g_cache_pod.load_all()

        if len(cluster_all) <= 0 or len(nodes_all) <= 0:
            return Response(generate_latest(registry), media_type="text/plain")

        def process_metrics(metrics_all, score_labels, metric_labels,
                            health_score, health_metric, cache):
            for item, results in metrics_all.items():
                metrics = json.loads(results)
                # the last element is the health score
                for metric in metrics:
                    if metric["type"] == ScoreType.MetricScore.value:
                        labels = [metric["labels"][label]
                                  for label in metric_labels[:-1]] + ["score"]
                        # return score of each metric
                        health_metric.labels(*labels).set(metric["score"])
                        # return value of each metric
                        labels[-1] = "value"
                        health_metric.labels(*labels).set(metric["value"])
                    elif metric["type"] == ScoreType.MetricTypeScore.value:
                        labels = [metric["labels"][label]
                                  for label in score_labels]
                        # return score of each metric
                        health_score.labels(*labels).set(metric["score"])
                    elif metric["type"] == ScoreType.InstanceScore.value:
                        labels = [metric["labels"][label]
                                  for label in score_labels[:-1]] + ["total"]
                        # return score of each metric
                        health_score.labels(*labels).set(metric["score"])

                # delete metrics from redis
                cache.delete(item)

        process_metrics(cluster_all, CLUSTER_HEALTH_SCORE_LABEL,
                        CLUSTER_HEALTH_METRIC_LABEL,
                        cluster_health_score,
                        cluster_health_metric, g_cache_cluster)
        process_metrics(nodes_all, NODE_HEALTH_SCORE_LABEL,
                        NODE_HEALTH_METRIC_LABEL,
                        node_health_score,
                        node_health_metric, g_cache_instance)
        process_metrics(pods_all, POD_HEALTH_SCORE_LABEL,
                        POD_HEALTH_METRIC_LABEL,
                        pod_health_score,
                        pod_health_metric, g_cache_pod)

    except Exception as e:
        logger.error("Exception: ", e)
        raise HTTPException(status_code=400, detail=str(e))
    finally:
        g_cache_cluster.clean()
        g_cache_instance.clean()
        g_cache_pod.clean()

    return Response(generate_latest(registry), media_type="text/plain")