import inspect
import app.models as models
from logging.config import fileConfig
from sqlalchemy import engine_from_config, Table
from sqlalchemy import pool
from app.models import Base
from alembic import context
from conf.settings import YAML_CONFIG, SQLALCHEMY_DATABASE_URL

##################################################################
# Load yaml config first
##################################################################
mysql_config = YAML_CONFIG.get_server_config().db.mysql

##################################################################
# Scan models
##################################################################
service_tables = []
for name, data in inspect.getmembers(models):
    if inspect.isclass(data):
        if data.__module__ != "app.models":
            continue
        if "__tablename__" in data.__dict__:
            service_tables.append(data.__dict__["__tablename__"])
        elif "__table__" in data.__dict__:
            service_tables.append(data.__dict__["__table__"])
    elif isinstance(data, Table):
        service_tables.append(name)

# this is the Alembic Config object, which provides
# access to the values within the .ini file in use.
config = context.config

# Interpret the config file for Python logging.
# This line sets up loggers basically.
if config.config_file_name is not None:
    fileConfig(config.config_file_name)

# Update mysql config according config.yml
config.set_main_option(
    "sqlalchemy.url",
    SQLALCHEMY_DATABASE_URL
)

# add your model's MetaData object here
# for 'autogenerate' support
# from myapp import mymodel
# target_metadata = mymodel.Base.metadata
target_metadata = Base.metadata

# other values from the config, defined by the needs of env.py,
# can be acquired:
# my_important_option = config.get_main_option("my_important_option")
# ... etc.

def include_object(object, name, type_, reflected, compare_to):
    if type_ == "table" and name not in service_tables:
        return False
    return True


def run_migrations_offline():
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    """
    url = config.get_main_option("sqlalchemy.url")
    context.configure(
        url=url,
        target_metadata=target_metadata,
        literal_binds=True,
        include_object=include_object,
        version_table="cmg_version",
        dialect_opts={"paramstyle": "named"},
    )

    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online():
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    connectable = engine_from_config(
        config.get_section(config.config_ini_section),
        prefix="sqlalchemy.",
        poolclass=pool.NullPool,
    )

    with connectable.connect() as connection:
        context.configure(
            connection=connection, target_metadata=target_metadata,
            include_object=include_object,
            version_table="cmg_version"
        )

        with context.begin_transaction():
            context.run_migrations()


if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
