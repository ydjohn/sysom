from abc import abstractmethod
import traceback
from pandas.core.api import DataFrame as DataFrame
from metric_reader.metric_reader import MetricReader
from metric_reader.result import MetricResult
from metric_reader.task import InstantQueryTask, RangeQueryTask
import conf.settings as settings
from clogger import logger
import pandas as pd
from typing import List


def aggravate_pod(result: MetricResult):
    time_series_list = []
    for item in result.data:
        df = pd.DataFrame(
            {
                "ts": pd.to_datetime([int(val[0]) for val in item.values], unit="s"),
                "value": [float(val[1]) for val in item.values],
            }
        )
        df.set_index("ts", inplace=True)
        df = df.resample("15S").ffill()
        time_series_list.append(df)
    df_resample = pd.concat(time_series_list, axis=1).reset_index()
    df_resample["metric"] = df_resample.drop("ts", axis=1).sum(axis=1)
    df_resample["ts"] = df_resample["ts"].apply(lambda x: x.timestamp())
    return df_resample[["ts", "metric"]]


def parse_aggregate_result(result: MetricResult) -> float:
    try:
        if result.code != 0:
            raise Exception(f"result: code={result.code} msg=({result.err_msg})")
        if len(result.data) == 0:
            raise Exception(f"Query no data.")

        return float(result.data[0].to_dict()["value"][1])
    except Exception as e:
        logger.error(e)
        return settings.DEFAULT_AGG_RESULT


class Table:
    def __init__(self, reader: MetricReader, cluster: str, instance: str) -> None:
        self._reader = reader
        self._cluster = cluster
        self._instance = instance
        self._table_name = None


class ResourceTable(Table):
    def __init__(
        self, reader: MetricReader, cluster: str, instance: str, resource: str, tag: str
    ) -> None:
        Table.__init__(self, reader, cluster, instance)
        self._resource = resource
        self._tag = tag


class PredictTable(ResourceTable):
    """The table that get the history data for model training.

    Args:
        Table (class): The class contain base attributes.
    """

    def __init__(
        self,
        reader: MetricReader,
        cluster: str,
        instance: str,
        resource: str,
        tag: str,
        max: float,
    ) -> None:
        """Init the PredictTable.

        Args:
            reader (MetricReader): Prometheus reader
            cluster (str): cluster id
            instance (str): instance id
            resource (str): resource type. reading conf/colocation_settings.py
            tag (str): aggravate level. reading conf/colocation_settings.py
            max (float): the max val for this <resource>. eg. for cpu is nr_cpu * 100, for memory is nr_byte.
        """
        ResourceTable.__init__(self, reader, cluster, instance, resource, tag)
        self._max = max

    @abstractmethod
    def range_query(self, start_time: float, end_time: float) -> pd.DataFrame:
        """query the range metric, process it for model training

        Args:
            start_time (float): range start time
            end_time (float): range end time

        Returns:
            pd.DataFrame: must include colmuns=['ts', 'metric] and all 'metric' must in [0, 100]
        """
        pass


class AllocationTable(ResourceTable):
    def __init__(
        self,
        reader: MetricReader,
        cluster: str,
        instance: str,
        resource: str,
        tag: str,
        max: float,
    ) -> None:
        ResourceTable.__init__(self, reader, cluster, instance, resource, tag)
        self._max = max

    @abstractmethod
    def query_allocation(
        self,
    ) -> float:
        """get the resource allocation percent

        Returns:
            float: must in [0, 100]
        """
        pass


class MaxTable(ResourceTable):
    def __init__(
        self, reader: MetricReader, cluster: str, instance: str, resource: str, tag: str
    ) -> None:
        ResourceTable.__init__(self, reader, cluster, instance, resource, tag)

    @abstractmethod
    def query_max(
        self,
    ) -> float:
        """get the resource max quota

        Returns:
            float: follow the resource type, the value will be used in normalized.
        """


class ProcCpuTotalTable(PredictTable):
    def __init__(
        self,
        reader: MetricReader,
        cluster: str,
        instance: str,
        resource: str,
        tag: str,
        max: float,
    ) -> None:
        PredictTable.__init__(self, reader, cluster, instance, resource, tag, max)
        self._table_name = settings.TABLE_PROC_CPU_TOTAL

    def range_query(self, start_time: float, end_time: float) -> pd.DataFrame:
        df = pd.DataFrame()
        task = RangeQueryTask(
            self._table_name, start_time=start_time, end_time=end_time
        ).append_equal_filter("instance", self._instance)

        result = self._reader.range_query([task])
        try:
            if result.code != 0:
                raise Exception(
                    f"Query {self._table_name} Failed. args: instance={self._instance} range={end_time-start_time:.1f} result: code=({result.code}) msg=({result.err_msg})"
                )

            if len(result.data) == 0:
                logger.warning(f"Query {self._table_name} no data.")
                return pd.DataFrame(settings.DEFAULT_PREDICT_QUERY_RESULT)

            df["ts"] = [float(val[0]) for val in result.data[0].values]
            for item in list(set(result.data)):
                if item.labels["mode"] in set(settings.PORC_CPU_USGAE):
                    df[item.labels["mode"]] = [
                        float(val[1]) / self._max * 100 for val in item.values
                    ]

            df = df.assign(metric=df[settings.PORC_CPU_USGAE].sum(axis=1))
            return df[["ts", "metric"]]
        except Exception as e:
            traceback.print_exc()
            logger.error(e)
            return pd.DataFrame(settings.DEFAULT_PREDICT_QUERY_RESULT)


class ContainerCpuacctStatTable(PredictTable):
    def __init__(
        self,
        reader: MetricReader,
        cluster: str,
        instance: str,
        level: str,
        resource: str,
        tag: str,
        max: float,
    ) -> None:
        PredictTable.__init__(self, reader, cluster, instance, resource, tag, max)
        self._level = level
        self._table_name = settings.TABLE_CONTAINER_CPUACCT_STAT
        self._value = "total"

    def range_query(self, start_time: float, end_time: float) -> pd.DataFrame:
        df = pd.DataFrame()
        task = (
            RangeQueryTask(self._table_name, start_time=start_time, end_time=end_time)
            .append_equal_filter("instance", self._instance)
            .append_equal_filter("bvt", self._level)
            .append_equal_filter("value", self._value)
            .append_equal_filter("container", "None")
        )
        result = self._reader.range_query([task])
        try:
            if result.code != 0:
                raise Exception(
                    f"Query {self._table_name} Failed. args: bvt={self._level} instance={self._instance} value={self._value} range={end_time-start_time:.1f} result: code=({result.code}) msg=({result.err_msg})"
                )

            if len(result.data) == 0:
                logger.warning(f"Query {self._table_name} no data.")
                return pd.DataFrame(settings.DEFAULT_PREDICT_QUERY_RESULT)

            df = aggravate_pod(result)
            df["metric"] = df["metric"] / self._max * 100
            return df[["ts", "metric"]]
        except Exception as e:
            traceback.print_exc()
            logger.error(e)
            return pd.DataFrame(settings.DEFAULT_PREDICT_QUERY_RESULT)

    def query_history_max(
        self, ns: str, pod: str, con: str, end: float, interval: str
    ) -> float:
        task = (
            InstantQueryTask(self._table_name, end, "max_over_time", interval)
            .append_equal_filter("instance", self._instance)
            .append_equal_filter("namespace", ns)
            .append_equal_filter("pod", pod)
            .append_equal_filter("container", con)
            .append_equal_filter("value", self._value)
        )
        result = self._reader.instant_query([task])
        data = parse_aggregate_result(result)
        if data is None:
            logger.error(
                f"No avail data: Query table={self._table_name} agg=max_over_time interval={interval} agg_val={None} ins={self._instance} ns={ns} pod={pod} con={con} value={self._value}"
            )
        return data

    def query_cpu_time(self, start, end) -> List[dict]:
        # query all pod
        task = (
            RangeQueryTask(self._table_name, start, end)
            .append_equal_filter("instance", self._instance)
            .append_equal_filter("bvt", settings.TAG_LS)
            .append_equal_filter("container", "None")
            .append_wildcard_filter("value", "user|system")
        )
        result = self._reader.range_query([task])
        try:
            if result.code != 0:
                raise Exception(
                    f"Query {self._table_name} Failed. result: code=({result.code}) msg=({result.err_msg})"
                )
            pod_info = {}
            for item in result.data:
                labels = item["labels"]
                values = item["values"]
                key = f"{labels['namespace']}-{labels['pod']}-{labels['container']}"
                if key not in pod_info.keys():
                    pod_info[key] = {
                        "ns": labels["namespace"],
                        "pod": labels["pod"],
                        "con": labels["container"],
                    }
                pod_info[key][labels["value"]] = values
            return [val for val in pod_info.values()]
        except Exception as e:
            logger.error(e)
            return []


class ContainerCfsQuotaTable(AllocationTable):
    def __init__(
        self,
        reader: MetricReader,
        cluster: str,
        instance: str,
        resource: str,
        tag: str,
        max: float,
    ) -> None:
        AllocationTable.__init__(self, reader, cluster, instance, resource, tag, max)
        self._table_name = settings.TABLE_CONTAINER_CFS_QUOTA
        self._level = settings.TAG_LS
        self._value = "quota_ratio"
        self._max = max

    def query_latest_quota_ratio(self, namespace, pod, con) -> float:
        task = (
            InstantQueryTask(self._table_name, None)
            .append_equal_filter("bvt", self._level)
            .append_equal_filter("value", self._value)
            .append_equal_filter("instance", self._instance)
            .append_equal_filter("namespace", namespace)
            .append_equal_filter("pod", pod)
            .append_equal_filter("container", con)
        )
        result = self._reader.instant_query([task])
        try:
            if result.code != 0:
                raise Exception(
                    f"Query {self._table_name} Failed. args: bvt={self._level} instance={self._instance} value={self._value}  result: code={result.code} msg=({result.err_msg})"
                )
            if len(result.data) == 0:
                raise Exception(f"Query {self._table_name} no data")
            return float(result.data[0].value[1])

        except Exception as e:
            traceback.print_exc()
            logger.error(e)
            return settings.DEFAULT_ALLOCAITON_RESULT

    def query_allocation(self) -> float:
        """calculate the LS CPU allocation quota

        Returns:
            float: sum of all LS Pod CPU quota_ratio, and normalize the value to the range of [0, 60].
        """
        # TODO use the pod level quota ratio, handle one pod with multi containers
        sum_val = 0
        task = (
            InstantQueryTask(self._table_name, None)
            .append_equal_filter("bvt", self._level)
            .append_equal_filter("value", self._value)
            .append_equal_filter("instance", self._instance)
            .append_equal_filter("container", "None")
        )
        result = self._reader.instant_query([task])
        try:
            if result.code != 0:
                raise Exception(
                    f"Query {self._table_name} Failed. args: bvt={self._level} instance={self._instance} value={self._value}  result: code={result.code} msg=({result.err_msg})"
                )

            if len(result.data) == 0:
                raise Exception(f"Query {self._table_name} no data")

            for item in result.data:
                pod_limit = float(item.to_dict()["value"][1])
                if pod_limit >= self._max:
                    from lib.pod_state import pod_mgr

                    labels = item.to_dict()["labels"]
                    pod_limit = pod_mgr.query_max(
                        self._cluster,
                        self._instance,
                        labels["namespace"],
                        labels["pod"],
                        labels["container"],
                        settings.RESOURCE_CPU,
                    )
                    if pod_limit is None:
                        logger.error(
                            f"get the history max cpu replace the unlimited cpu error. ins={self._instance} ns={labels['namespace']} pod={labels['pod']} con={labels['container']} pod_limit={pod_limit} self._max={self._max}"
                        )
                        pod_limit = 0
                sum_val += pod_limit * settings.POD_UNLIMITED_FACTOR

            logger.warning(
                f"cpu allocation_rate({sum_val / self._max * 100}) = sum_val({sum_val}) / max({self._max}) * 100"
            )
            """ not allowed return val more than 60 """
            return min(settings.CPU_MAX_ALLOCATION_PRECENT, sum_val / self._max * 100)
        except Exception as e:
            traceback.print_exc()
            logger.error(e)
            return settings.DEFAULT_ALLOCAITON_RESULT


class ProcCpusTable(MaxTable):
    def __init__(
        self, reader: MetricReader, cluster: str, instance: str, resource: str, tag: str
    ) -> None:
        MaxTable.__init__(self, reader, cluster, instance, resource, tag)
        self._table_name = settings.TABLE_PROC_CPUS
        self._mode = "total"
        self._total = None

    def query_max(self) -> float:
        if self._total is not None:
            return self._total

        task = (
            InstantQueryTask(self._table_name, None, "count")
            .append_equal_filter("mode", self._mode)
            .append_equal_filter("instance", self._instance)
        )
        result = self._reader.instant_query([task])
        nr_cpu = parse_aggregate_result(result)
        if nr_cpu is not None:
            self._total = nr_cpu * 100
        else:
            logger.error(
                f"No avail data: Query table={self._table_name} agg=count interval=None agg_val={None} ins={self._instance} ns={None} pod={None} con={None} value={None}"
            )

        return self._total


class ProcMeminfoTable(MaxTable, PredictTable):
    def __init__(
        self,
        reader: MetricReader,
        cluster: str,
        instance: str,
        resource: str,
        tag: str,
        max: float,
    ) -> None:
        MaxTable.__init__(self, reader, cluster, instance, resource, tag)
        PredictTable.__init__(self, reader, cluster, instance, resource, tag, max)
        self._table_name = settings.TABLE_PROC_MEMINFO
        self._max_value = "MemTotal"
        self._avail_value = "MemAvailable"
        self._total = None

    def range_query(self, start_time: float, end_time: float) -> DataFrame:
        df = pd.DataFrame()
        task = (
            RangeQueryTask(self._table_name, start_time=start_time, end_time=end_time)
            .append_equal_filter("instance", self._instance)
            .append_equal_filter("value", self._avail_value)
        )
        result = self._reader.range_query([task])
        try:
            if result.code != 0:
                raise Exception(
                    f"Query {self._table_name} Failed. args: instance={self._instance} value={self._avail_value} range={end_time-start_time:.1f} result: code=({result.code}) msg=({result.err_msg})"
                )
            if len(result.data) != 1:
                raise Exception(
                    f"Query {self._table_name} data num is incorrect. size=({len(result.data)}) args: instance={self._instance} value={self._avail_value} range={end_time-start_time:.1f}"
                )

            df["ts"] = [float(val[0]) for val in result.data[0].values]
            """ calculate the usage_rate = (total - available) / total * 100"""
            df["metric"] = [
                (self._max - float(val[1]) * 1024) / self._max * 100
                for val in result.data[0].values
            ]
            return df[["ts", "metric"]]
        except Exception as e:
            traceback.print_exc()
            logger.error(e)
            return pd.DataFrame(settings.DEFAULT_PREDICT_QUERY_RESULT)

    def query_max(self) -> float:
        if self._total is not None:
            logger.debug(f"{self._table_name} total_max={self._total}")
            return self._total
        task = (
            InstantQueryTask(self._table_name, None)
            .append_equal_filter("instance", self._instance)
            .append_equal_filter("value", self._max_value)
        )
        result = self._reader.instant_query([task])
        try:
            if result.code != 0:
                raise Exception(
                    f"Query {self._table_name} Failed. args: mode={self._mode}  result: code=({result.code}) msg=({result.err_msg})"
                )
            if len(result.data) == 0:
                raise Exception(f"Query {self._table_name} no data.")

            """ calculate the total memory in bytes """
            self._total = float(result.data[0].to_dict()["value"][1]) * 1024
            return self._total
        except Exception as e:
            traceback.print_exc()
            logger.error(e)
            return settings.DEFAULT_MAX_RESULT


class ContainerMemutilTable(PredictTable, AllocationTable):
    def __init__(
        self,
        reader: MetricReader,
        cluster: str,
        instance: str,
        resource: str,
        tag: str,
        max: float,
        level: str,
    ) -> None:
        PredictTable.__init__(self, reader, cluster, instance, resource, tag, max)
        AllocationTable.__init__(self, reader, cluster, instance, resource, tag, max)
        self._table_name = settings.TABLE_CONTAINER_MEMUTIL
        self._usage_value = "usage"
        self._limit_value = "limit"
        self._level = level

    def range_query(self, start_time: float, end_time: float) -> DataFrame:
        task = (
            RangeQueryTask(self._table_name, start_time=start_time, end_time=end_time)
            .append_equal_filter("instance", self._instance)
            .append_equal_filter("value", self._usage_value)
            .append_equal_filter("bvt", self._level)
            .append_equal_filter("container", "None")
        )
        result = self._reader.range_query([task])

        try:
            if result.code != 0:
                raise Exception(
                    f"Query {self._table_name} Failed. args: instance={self._instance} value={self._usage_value} range={end_time-start_time:.1f} result: code=({result.code}) msg=({result.err_msg})"
                )

            if len(result.data) == 0:
                logger.warning(f"Query {self._table_name} no data.")
                return pd.DataFrame(settings.DEFAULT_PREDICT_QUERY_RESULT)

            df = aggravate_pod(result)
            df["metric"] = df["metric"] / self._max * 100
            return df[["ts", "metric"]]
        except Exception as e:
            traceback.print_exc()
            logger.error(e)
            return pd.DataFrame(settings.DEFAULT_PREDICT_QUERY_RESULT)

    def query_history_max(
        self, ns: str, pod: str, con: str, end: float, interval: str
    ) -> float:
        task = (
            InstantQueryTask(self._table_name, end, "max_over_time", interval)
            .append_equal_filter("instance", self._instance)
            .append_equal_filter("value", self._limit_value)
            .append_equal_filter("namespace", ns)
            .append_equal_filter("pod", pod)
            .append_equal_filter("container", con)
        )
        result = self._reader.instant_query([task])
        data = parse_aggregate_result(result)
        if data is None:
            logger.error(
                f"No avail data: Query table={self._table_name} agg=max_over_time interval={interval} agg_val={None} ins={self._instance} ns={ns} pod={pod} con={con} value={self._limit_value}"
            )
        return data

    def query_allocation(self) -> float:
        sum_val = 0
        task = (
            InstantQueryTask(self._table_name, None)
            .append_equal_filter("bvt", self._level)
            .append_equal_filter("value", self._limit_value)
            .append_equal_filter("instance", self._instance)
            .append_equal_filter("container", "None")
        )
        result = self._reader.instant_query([task])
        try:
            if result.code != 0:
                raise Exception(
                    f"Query {self._table_name} Failed. args: bvt={self._level} instance={self._instance} value={self._limit_value} container=None result: code={result.code} msg=({result.err_msg})"
                )
            if len(result.data) == 0:
                raise Exception(
                    f"Query {self._table_name} no data.  args: bvt={self._level} instance={self._instance} value={self._limit_value} container=None "
                )

            for item in result.data:
                pod_limit = float(item.to_dict()["value"][1])
                labels = item.to_dict()["labels"]
                if pod_limit >= self._max:
                    from lib.pod_state import pod_mgr

                    pod_limit = pod_mgr.query_max(
                        self._cluster,
                        self._instance,
                        labels["namespace"],
                        labels["pod"],
                        labels["container"],
                        settings.RESOURCE_MEMORY,
                    )
                sum_val += pod_limit * settings.POD_UNLIMITED_FACTOR
            """ not allowed return val more than 100 """
            return min(settings.MEM_MAX_ALLOCATION_PERCENT, sum_val / self._max * 100)
        except Exception as e:
            traceback.print_exc()
            logger.error(e)
            return settings.DEFAULT_ALLOCAITON_RESULT


class ContainerPmuEvents(Table):
    def __init__(self, reader: MetricReader, cluster: str, instance: str) -> None:
        Table.__init__(self, reader, cluster, instance)
        self._table_name = settings.TABLE_CONTANINER_PMU_EVENTS
        self._value = "CPI"

    def query_history_stddev(
        self, ns: str, pod: str, con: str, end: float, interval: str
    ) -> float:
        task = (
            InstantQueryTask(self._table_name, end, "stddev_over_time", interval)
            .append_equal_filter("instance", self._instance)
            .append_equal_filter("namespace", ns)
            .append_equal_filter("pod", pod)
            .append_equal_filter("container", con)
            .append_equal_filter("value", self._value)
        )
        result = self._reader.instant_query([task])
        data = parse_aggregate_result(result)
        if data is None:
            logger.error(
                f"No avail data: Query table={self._table_name} agg=stddev_over_time interval={interval} agg_val={None} ins={self._instance} ns={ns} pod={pod} con={con} value={self._value}"
            )
        return data

    def query_history_mean(
        self, ns: str, pod: str, con: str, end: float, interval: str
    ) -> float:
        task = (
            InstantQueryTask(self._table_name, end, "avg_over_time", interval)
            .append_equal_filter("instance", self._instance)
            .append_equal_filter("namespace", ns)
            .append_equal_filter("pod", pod)
            .append_equal_filter("container", con)
            .append_equal_filter("value", self._value)
        )
        result = self._reader.instant_query([task])
        data = parse_aggregate_result(result)
        if data is None:
            logger.error(
                f"No avail data: Query table={self._table_name} agg=avg_over_time interval={interval} agg_val={None} ins={self._instance} ns={ns} pod={pod} con={con} value={self._value}"
            )
        return data

    def query_lastest_cpi(self, start_time, end_time) -> List[dict]:
        container_list = []
        task = (
            RangeQueryTask(self._table_name, start_time=start_time, end_time=end_time)
            .append_equal_filter("instance", self._instance)
            .append_equal_filter("bvt", settings.TAG_LS)
            .append_equal_filter("value", self._value)
        )

        result = self._reader.range_query([task])
        try:
            if result.code != 0:
                raise Exception(
                    f"Query {self._table_name} Failed. args: instance={self._instance} result: code={result.code} msg=({result.err_msg})"
                )
            for item in result.data:
                labels = item.to_dict()["labels"]
                if labels["container"] != "None":
                    container_list.append(
                        {
                            "ns": labels["namespace"],
                            "pod": labels["pod"],
                            "con": labels["container"],
                            "data": item.values,
                        }
                    )
            return container_list
        except Exception as e:
            logger.error(e)
            return []


class ContaierCfsStatistics(Table):
    def __init__(self, reader: MetricReader, cluster: str, instance: str) -> None:
        Table.__init__(self, reader, cluster, instance)
        self._table_name = settings.TABLE_CONTANINER_CFS_STATISTICS

    def query_serve_util_rate(self, start_time, end_time) -> List[dict]:
        container_info = {}
        task = (
            RangeQueryTask(self._table_name, start_time=start_time, end_time=end_time)
            .append_equal_filter("instance", self._instance)
            .append_equal_filter("bvt", settings.TAG_LS)
            .append_equal_filter("value", "serveutil")
        )

        result = self._reader.range_query([task])
        try:
            if result.code != 0:
                raise Exception(
                    f"Query {self._table_name} Failed. args: instance={self._instance} result: code={result.code} msg=({result.err_msg})"
                )
            for item in result.data:
                labels = item.to_dict()["labels"]
                key = f"{labels['namespace']}-{labels['pod']}-{labels['container']}"
                if labels["container"] != "None":
                    container_info[key] = {
                        "ns": labels["namespace"],
                        "pod": labels["pod"],
                        "con": labels["container"],
                        "serveutil": [val for val in item.values],
                    }
            return [v for v in container_info.values()]
        except Exception as e:
            logger.error(e)
            return []

    def query_p99_one_week(self, ns: str, pod: str, con: str, end: float) -> float:
        """get the p99 serve util, because the serveutil bigger the status better,
            so we get the p01 as the threshold

        Args:
            ns (str): _description_
            pod (str): _description_
            con (str): _description_
            end (float): _description_

        Returns:
            float: _description_
        """
        task = (
            InstantQueryTask(
                metric_name=self._table_name,
                time=end,
                aggregation="quantile_over_time",
                interval="7d",
                aggregation_val="0.01",
            )
            .append_equal_filter("instance", self._instance)
            .append_equal_filter("namespace", ns)
            .append_equal_filter("pod", pod)
            .append_equal_filter("container", con)
            .append_equal_filter("value", "serveutil")
        )

        result = self._reader.instant_query([task])
        data = parse_aggregate_result(result)
        if data is None:
            logger.error(
                f"No avail data: Query table={self._table_name} agg=quantile_over_time interval=7d agg_val=0.01 ins={self._instance} ns={ns} pod={pod} con={con} value=serveutil"
            )
        return data
