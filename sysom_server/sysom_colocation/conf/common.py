# -*- coding: utf-8 -*- #
"""
Time                2023/12/20 19:47
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                common.py
Description:
"""
from pathlib import Path
from sysom_utils import ConfigParser, SysomFramework

BASE_DIR = Path(__file__).resolve().parent.parent

##################################################################
# Load yaml config first
##################################################################
YAML_GLOBAL_CONFIG_PATH = f"{BASE_DIR.parent.parent}/conf/config.yml"
YAML_SERVICE_CONFIG_PATH = f"{BASE_DIR}/config.yml"

YAML_CONFIG = ConfigParser(YAML_GLOBAL_CONFIG_PATH, YAML_SERVICE_CONFIG_PATH)

mysql_config = YAML_CONFIG.get_server_config().db.mysql
prometheus_config = YAML_CONFIG.get_server_config().db.prometheus
service_config = YAML_CONFIG.get_service_config()

SysomFramework.init(YAML_CONFIG)

##################################################################
# fastapi config
##################################################################
SQLALCHEMY_DATABASE_URL = (
    f"{mysql_config.dialect}+{mysql_config.engine}://{mysql_config.user}:{mysql_config.password}@"
    f"{mysql_config.host}:{mysql_config.port}/{mysql_config.database}"
)

PROMETHEUS_DATABASE_URL = f"prometheus://{prometheus_config.host}:{prometheus_config.port}"

