#################################################################################
# Cache Settings
#################################################################################

# the realtime predict metric cache
CACHE_RT = "colocation_instance_rt_cache"
# the future predict metric cache
CACHE_FUTURE = "colocation_instance_future_cache"

#################################################################################
# Table Settings
#################################################################################

# calculate the cpu usage by sum the labels metric
PORC_CPU_USGAE = ["softirq", "user", "sys", "hardirq", "nice"]

# the table name
TABLE_PROC_CPU_TOTAL = "sysom_proc_cpu_total"
TABLE_CONTAINER_CPUACCT_STAT = "sysom_container_cpuacct_stat"
TABLE_CONTAINER_CFS_QUOTA = "sysom_container_cfs_quota"
TABLE_PROC_CPUS = "sysom_proc_cpus"
TABLE_PROC_MEMINFO = "sysom_proc_meminfo"
TABLE_CONTAINER_MEMUTIL = "sysom_container_memUtil"
TABLE_CONTANINER_PMU_EVENTS = "sysom_container_pmu_events"
TABLE_CONTANINER_CFS_STATISTICS = "sysom_container_cfs_statis"

# the metric label
CLUSTER_LABEL = "cluster"
NODE_LABEL = "instance"
POD_LABEL = "pod"

# the default return value when range_query get no data or exception
DEFAULT_PREDICT_QUERY_RESULT = {"ts": [], "metric": []}
DEFAULT_MAX_RESULT = None
DEFAULT_AGG_RESULT = None
DEFAULT_ALLOCAITON_RESULT = None

#################################################################################
# Model Settings
#################################################################################

SEC_PER_MINUTE = 60
SEC_PER_HOUR = 60 * SEC_PER_MINUTE
# seconds per day
SEC_PER_DAY = 24 * SEC_PER_HOUR
# first init model will learn DEFAULT_HISTORY_SPAN_SECS hiistory data.
DEFAULT_HISTORY_SPAN_SECS = 14 * SEC_PER_DAY
DEFAULT_HISTORY_MAX_UPDATE_INTERVAl = SEC_PER_HOUR
DEFAULT_FIRST_CHECK_INTERVAL = 10 * SEC_PER_MINUTE
DEFAULT_CPI2_PERIOD = 5 * SEC_PER_MINUTE
DEFAULT_CPI2_ANORMAL_THRESHOLD = 3
# predict resource type
RESOURCE_CPU = "CPU"
RESOURCE_MEMORY = "MEMORY"
RESOURCE_IO = "IO"

# precit recource level
TAG_LS = "LS"
TAG_BE = "BE"
TAG_NOR = "NOR"
TAG_ALL = "ALL"
TAG_OTHER = "OTHER"

# predict category
CATEGORY_PERDICT = "predict"
CATEGORY_SLACK = "slack"
CATEGORY_REAL = "real"

# period predict algo argument, control the nr_window
ALGO_WINDOW_MINUTES = 60
# control the nr_slot
ALGO_SLOT_INTERVAl = 0.1
# control the history contribute
ALGO_HALF_LIFE = 1024

#################################################################################
# Cluster Settings
#################################################################################
# if NO_CLUSTER_LABEL is true, we consider there is one cluster
NO_CLUSTER_LABEL = True

#################################################################################
# Slack Settings
#################################################################################
SLACK_FACTOR = 0.8
CPU_MAX_ALLOCATION_PRECENT = 60
MEM_MAX_ALLOCATION_PERCENT = 85
POD_UNLIMITED_FACTOR = 1.01
