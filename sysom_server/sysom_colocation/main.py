# -*- coding: utf-8 -*- #
"""
Time                2023/12/20 19:47
Author:             weizhen (ZouTao)
Email               wodemia@linux.alibaba.com
File                main.py
Description:
"""
from clogger import logger
from fastapi import FastAPI, HTTPException, Response
from app.routers import health
from prometheus_client import Gauge, CollectorRegistry, generate_latest
from conf.settings import YAML_CONFIG
import conf.settings as settings
from app.worker.period_predict_worker import PeriodPredictWorker
from app.worker.cpi_worker import CPIWorker
from app.worker.serve_util_worker import ServeUtilWorker
from app.worker.sys_worker import SysWorker
from sysom_utils import CmgPlugin, SysomFramework
import json


app = FastAPI()
g_cache_rt = SysomFramework.gcache(settings.CACHE_RT)
g_cache_future = SysomFramework.gcache(settings.CACHE_FUTURE)
CLUSTER_COLOCATION_LABEL_RT = ["instance", "resource", "tag", "category"]
CLUSTER_COLOCATION_LABEL_FUTURE = ["instance", "resource", "tag", "category", "future"]

registry = CollectorRegistry()

colocation_node_predict_rt = Gauge(
    "sysom_colocation_node_predict_rt",
    "sysom_colocation_node_predict_rt",
    CLUSTER_COLOCATION_LABEL_RT,
    registry=registry,
)

colocation_node_predict_future = Gauge(
    "sysom_colocation_node_predict_future",
    "sysom_colocation_node_predict_future",
    CLUSTER_COLOCATION_LABEL_FUTURE,
    registry=registry,
)


app.include_router(health.router, prefix="/api/v1/colocation/health")
# app.include_router(health.router, prefix="/api/v1/colocation/person")


#############################################################################
# Write your API interface here, or add to app/routes
#############################################################################


def init_framwork():
    SysomFramework.init(YAML_CONFIG).load_plugin_cls(CmgPlugin).start()
    logger.info("SysomFramework init finished!")


@app.on_event("startup")
async def on_start():
    init_framwork()
    g_cache_rt.clean()
    g_cache_future.clean()
    PeriodPredictWorker().start()
    logger.info(f"混部资源水位预测服务已启动.")
    CPIWorker().start()
    logger.info(f"混部干扰检测-CPI干扰检测服务已启动.")
    SysWorker().start()
    logger.info(f"混部干扰检测-Sys干扰检测服务已启动.")
    ServeUtilWorker().start()
    logger.info(f"混部干扰检测-CFS满足率干扰检测服务已启动.")

    #############################################################################
    # Perform some microservice initialization operations over here
    #############################################################################


@app.on_event("shutdown")
async def on_shutdown():
    pass


@app.get("/metrics")
def prometheus_get_metrics():
    def process_cache(cache: SysomFramework.gcache, gauge: Gauge, labels: list):
        metrics_all = cache.load_all()

        if len(metrics_all) <= 0:
            return

        for _, results in metrics_all.items():
            logger.debug(f"values={results}")
            metrics = json.loads(results)
            gauge.labels(*[metrics[label] for label in labels]).set(metrics["value"])

    try:
        process_cache(
            g_cache_rt, colocation_node_predict_rt, CLUSTER_COLOCATION_LABEL_RT
        )
        process_cache(
            g_cache_future,
            colocation_node_predict_future,
            CLUSTER_COLOCATION_LABEL_FUTURE,
        )
    except Exception as e:
        logger.error(e)
    finally:
        return Response(generate_latest(registry), media_type="text/plain")
