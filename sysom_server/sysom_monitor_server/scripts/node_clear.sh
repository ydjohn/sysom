#!/bin/bash -x

RESOURCE_DIR=${NODE_HOME}/${SERVICE_NAME}

clear_node_exporter() {
    systemctl stop node_exporter
    rm -f /usr/lib/systemd/system/node_exporter.service
}

clear_sysak() {
    rpm -qa | grep sysak
    if [ $? -eq 0 ]; then
        yum erase -y `rpm -qa | grep sysak`
    fi
}

main() {
    clear_node_exporter
    clear_sysak
    rm -rf ${RESOURCE_DIR}
    # bash -x raptor_profiling_clear.sh
    exit 0
}

main
