import aiohttp
from fastapi import APIRouter, Response
from sysom_utils import SysomFramework

router = APIRouter()
g_client = SysomFramework.gclient("sysom_api")


@router.get("/sd_config")
async def search_grafana(response: Response):
    res = g_client.get("/api/v1/host/").json()
    return [
        {
            "targets": [f"{item['ip']}:9100" for item in res["data"]] + [f"{item['ip']}:8400" for item in res["data"]],
            "labels": {
                "source": "sysom_monitor_server"
            }
        }
    ]
