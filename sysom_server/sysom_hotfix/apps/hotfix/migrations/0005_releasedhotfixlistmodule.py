# Generated by Django 3.2.16 on 2023-11-16 06:14

from django.db import migrations, models
import lib.utils


class Migration(migrations.Migration):

    dependencies = [
        ('hotfix', '0004_auto_20231020_1552'),
    ]

    operations = [
        migrations.CreateModel(
            name='ReleasedHotfixListModule',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.CharField(default=lib.utils.human_datetime, max_length=20, verbose_name='创建时间')),
                ('deleted_at', models.CharField(max_length=20, null=True)),
                ('hotfix_id', models.CharField(max_length=50, verbose_name='hotfix id')),
                ('released_kernel_version', models.CharField(max_length=60, verbose_name='hotfix发布的内核版本')),
                ('serious', models.IntegerField(choices=[(0, '可选安装'), (1, '建议安装'), (2, '需要安装')], default=0, verbose_name='hotfix推荐安装级别')),
                ('description', models.TextField(default='', verbose_name='hotfix问题描述')),
                ('fix_system', models.IntegerField(choices=[(0, '调度'), (1, '内存'), (2, '网络'), (3, '存储'), (4, '其他')], default=0, verbose_name='涉及子系统')),
                ('released_time', models.DateTimeField(verbose_name='发布时间')),
                ('download_link', models.TextField(default='', verbose_name='hotfix下载链接')),
                ('deprecated', models.IntegerField(choices=[(0, '正常'), (1, '废弃')], default=0, verbose_name='该hotfix是否被废弃')),
                ('deprecated_info', models.TextField(default='', verbose_name='废弃原因或信息')),
                ('modified_time', models.DateTimeField(auto_now=True, verbose_name='记录修改时间')),
                ('modified_user', models.CharField(default='', max_length=20, null=True, verbose_name='用于记录最后一次修改的人')),
            ],
            options={
                'db_table': 'sys_released_hotfix',
                'ordering': ['-created_at'],
            },
        ),
    ]
