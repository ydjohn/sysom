from .base import DiagnosisPreProcessor, DiagnosisTask, DiagnosisJob
from sysom_utils import SysomFramework

# 创建一个 GClient 用于访问 sysom-api 服务
g_client = SysomFramework.gclient("sysom_rca")

# 调用 sysom-api 服务的接口获取主机列表
#res = g_client.get("/api/v1/host/")

# 输出主机列表
#print(res.json())




class PreProcessor(DiagnosisPreProcessor):
    """Command diagnosis

    Just invoke command in target instance and get stdout result

    Args:
        DiagnosisPreProcessor (_type_): _description_
    """

    def get_diagnosis_cmds(self, params: dict) -> DiagnosisTask:
        rca_type = params.get("rca_type", "rca")
        ts = params.get("time", "")
        base_item = params.get("base_item", "")
        machine_ip = params.get("instance", "")
        print ("\n\n",rca_type,ts,base_item,machine_ip,"\n\n")

        retrca = g_client.post("/api/v1/rca/rca_entry",json={"rca_type":rca_type,"timestamp":ts,"base_item":base_item,"machine_ip":machine_ip})
        print ("\n\n",retrca.json(),"\n\n")
        try:
            retrca_dict = retrca.json()
        except:
            retrca_dict = {"success":False,"errmsg":"rca_pre error!","dist":[]}
            pass
        return DiagnosisTask(
            # 离线模式不需要在节点上执行，所以此处 instance 和 cmd 指定为空即可
            jobs=[DiagnosisJob(instance="", cmd="")],
            # 开启离线模式
            offline_mode=True,
            # 返回离线诊断的结果（数组中的每一项，最后都会传递到 DiagnosisJobResult.stdout）
            offline_results=[
                retrca_dict
            ]
        )
