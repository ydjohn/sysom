"""
Time                2023/07/21 17:32
Author:             chenshiyan (shiyan)
Email               chenshiyan@linux.alibaba.com
File                rca_post.py
Description:
"""
from typing import List
from .base import DiagnosisJobResult, DiagnosisPostProcessor, PostProcessResult
import json


class PostProcessor(DiagnosisPostProcessor):
    def parse_diagnosis_result(self, results: List[DiagnosisJobResult]) -> PostProcessResult:
        ret = results[0].stdout
        print (ret)
        ret_code = 0
        ret_errmsg = ""
        try:
            if ret["success"] != True:
                ret_code = 1
                ret_errmsg = ret["errmsg"]
        except:
            ret_code = 2
            ret_errmsg = "rca_post error!"
            pass
        postprocess_result = PostProcessResult(
            code=ret_code,
            err_msg=ret_errmsg,
            result={}
        )
        md_msg = ""
        md_msg = "## 指标异常分析结果\n\n\n\n%s\n\n\n\n"%ret["sum_dict"]["ref_item_sum"]
        md_msg = "%s## 修复/进一步排查建议\n\n\n\n%s\n\n\n\n"%(md_msg,ret["sum_dict"]["fix_sum"])
        md_msg = "%s## 相似指标排序\n\n\n\n%s\n\n\n\n"%(md_msg,ret["sum_dict"]["ref_item_list"])
        md_msg = "%s## 相似指标图示\n\n\n\n![picture_url](%s)\n\n\n\n"%(md_msg,ret["pic_url"])
        postprocess_result.result = {
            "RcaResult": {"data": md_msg}
        }
        return postprocess_result
