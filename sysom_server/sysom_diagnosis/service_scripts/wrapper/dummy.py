from service_scripts.base import DiagnosisTask
from .base import DiagnosisPreProcessorPostWrapperBase

class DiagnosisPreProcessorPostWrapper(DiagnosisPreProcessorPostWrapperBase):
    """所有命令的执行结果导出到临时文件并cat出来

    Args:
        DiagnosisPreProcessorPostWrapperBase (_type_): _description_
    """
    def process(self, task_id: str, diagnosis_task: DiagnosisTask):
        for job in diagnosis_task.jobs:
            job.cmd = f"({job.cmd}) > /tmp/{job.instance}.txt && cat /tmp/{job.instance}.txt"