import json
from typing import List
from .base import DiagnosisJobResult, DiagnosisPostProcessor, PostProcessResult

class PostProcessor(DiagnosisPostProcessor):
    def parse_diagnosis_result(self, results: List[DiagnosisJobResult]) -> PostProcessResult:
        postprocess_result = PostProcessResult(
            code=0,
            err_msg="",
            result={}
        )
        
        results = results[0].stdout
        #结果字符串数据分割
        results = results.split("\n")
        #是否写入数据标识
        write_off = True
        index_flag = 0
        md_msg = '# 诊断结果'
        for msg in results:
            if "diagnosing report" in msg:
                index_flag = results.index(msg)
                write_off = False
            if not write_off:
                if "---" in msg:
                    continue
                md_msg = md_msg + "\n\n" + msg

        md_msg = md_msg + "\n\n" + "# 诊断过程"
        write_off = True
        for msg in results[:index_flag]:
            if "---" in msg:
                write_off = False
                msg = msg.replace("---", "")
                msg = "\n## %s\n" % msg
            if not write_off:
                md_msg = md_msg + "\n\n" + msg
        
        postprocess_result.result = {
            "procdiag_data": {"data": md_msg}
        }
        return postprocess_result
