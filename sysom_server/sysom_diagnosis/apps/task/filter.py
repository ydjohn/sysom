#!/usr/bin/python3
# -*- coding: utf-8 -*-
import json
import django_filters
from typing import List
from clogger import logger
from rest_framework.filters import BaseFilterBackend
from apps.task.models import JobModel


class TaskFilter(django_filters.FilterSet):
    service_name = django_filters.CharFilter(field_name="service_name")
    # https://stackoverflow.com/questions/58977818/how-to-use-django-filter-on-jsonfield
    channel = django_filters.CharFilter(field_name="params__channel")
    params = django_filters.CharFilter(method='params_filter')

    def params_filter(self, queryset, name, value):
        values: List[dict] = json.loads(value)
        params = {item['key']: item['value']  for item in values if item["value"]}
        return queryset.filter(params__contains=params) if value else queryset

    class Meta:
        model = JobModel
        fields = ["id", "task_id", "created_by__id", "status", "params"]


class IsOwnerFilterBackend(BaseFilterBackend):
    """
    Filter that only allows users to see their own objects.
    """

    def filter_queryset(self, request, queryset, view):
        if request.user is None and request.method == "GET":
            return queryset
        user_id = request.user.get("id", None)
        is_admin = request.user.get("is_admin", False)
        if is_admin:
            return queryset
        else:
            return queryset.filter(created_by=user_id)
