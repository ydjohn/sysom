# -*- coding: utf-8 -*- #
"""
Time                2022/11/14 14:32
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                models.py
Description:
"""
from sqlalchemy import Column, Integer, String, Text, JSON
from app.database import Base


###########################################################################
# Define databse model here
###########################################################################

# @reference https://fastapi.tiangolo.com/zh/tutorial/sql-databases/
# class Person(Base):
#     __tablename__ = "sys_person"
#     id = Column(Integer, primary_key=True)
#     name = Column(String(254), unique=True)
#     age = Column(Integer)


class NodeLog(Base):
    """
    节点日志
    """

    __tablename__ = "sys_node_log"
    instance = Column(String(32), comment="节点ID")
    event_type = Column(String(64), comment="异常类型")
    description = Column(Text, comment="日志描述")
    extra = Column(JSON, comment="日志额外信息")


class AuditLog(Base):
    """
    审计日志
    """

    __tablename__ = "sys_audit_log"
    methond = Column(String(32), comment="请求方法")
    ip = Column(String(32), comment="请求IP地址")
    path = Column(String(128), comment="路径")
    browser_agent = Column(String(256), comment="客户端信息")
    handler = Column(String(64), comment="处理视图")
    status = Column(Integer, comment="状态码")
    request_type = Column(String(32), comment="请求类型")
    user = Column(Integer, nullable=True, comment="操作用户")
