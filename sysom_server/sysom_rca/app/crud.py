# -*- coding: utf-8 -*- #
"""
Time                2023/08/02 14:32
Author:             shiyan
Email               chenshiyan@linux.alibaba.com
File                crud.py
Description:
"""
from typing import Optional
from sqlalchemy.orm import Session
from app import models, schemas

################################################################################################
# Define database crud here
################################################################################################

def create_rcacallrecord(db: Session, rcacallrecord: schemas.RcaCallRecord) -> models.RcaCallRecord:
    rcacallrecord_item = models.RcaCallRecord(**rcacallrecord.dict())
    db.add(rcacallrecord_item)
    db.commit()
    db.refresh(rcacallrecord_item)
    return rcacallrecord_item

def get_rcacallrecord_by_recordid(db: Session, recordid: str) -> Optional[models.RcaCallRecord]:
    return db.query(models.RcaCallRecord).filter(models.RcaCallRecord.recordid == recordid).first()

def update_or_create_rcacallrecord(db: Session, rcacallrecord: schemas.RcaCallRecord) -> models.RcaCallRecord:
    rcacallrecord_item = get_rcacallrecord_by_recordid(db, rcacallrecord.recordid)
    if rcacallrecord_item is None:
        rcacallrecord_item = create_rcacallrecord(db, rcacallrecord)
    else:
        rcacallrecord_item.url = rcacallrecord.url
        rcacallrecord_item.state = rcacallrecord.state
        rcacallrecord_item.timestamp = rcacallrecord.timestamp
        db.commit()

def update_rcacallrecord_state(db: Session, state:str, recordid: str) -> Optional[models.RcaCallRecord]:
    rcacallrecord_item = get_rcacallrecord_by_recordid(db, recordid)
    if rcacallrecord_item is not None:
        rcacallrecord_item.state = state
        db.commit()
    return rcacallrecord_item

def create_rcaitemsrecord(db: Session, rcaitemsrecord: schemas.RcaItemsRecord) -> models.RcaItemsRecord:
    rcaitemsrecord_item = models.RcaItemsRecord(**rcaitemsrecord.dict())
    db.add(rcaitemsrecord_item)
    db.commit()
    db.refresh(rcaitemsrecord_item)
    return rcaitemsrecord_item

def get_rcaitemsrecord_by_recordid(db: Session, recordid: str) -> Optional[models.RcaItemsRecord]:
    return db.query(models.RcaItemsRecord).filter(models.RcaItemsRecord.recordid == recordid).first()

def update_or_create_rcaitemsrecord(db: Session, rcaitemsrecord: schemas.RcaItemsRecord) -> models.RcaItemsRecord:
    rcaitemsrecord_item = get_rcaitemsrecord_by_recordid(db, rcaitemsrecord.recordid)
    if rcaitemsrecord_item is None:
        rcaitemsrecord_item = create_rcaitemsrecord(db, rcaitemsrecord)
    else:
        rcaitemsrecord_item.url = rcaitemsrecord.url
        rcaitemsrecord_item.state = rcaitemsrecord.state
        rcaitemsrecord_item.user = rcaitemsrecord.user
        rcaitemsrecord_item.time_occur = rcaitemsrecord.time_occur
        rcaitemsrecord_item.machine_ip = rcaitemsrecord.machine_ip
        rcaitemsrecord_item.time_start = rcaitemsrecord.time_start
        rcaitemsrecord_item.time_end = rcaitemsrecord.time_end
        rcaitemsrecord_item.metric_dict = rcaitemsrecord.metric_dict
        rcaitemsrecord_item.rca_conclusion = rcaitemsrecord.rca_conclusion
        rcaitemsrecord_item.final_conclusion = rcaitemsrecord.final_conclusion
        db.commit()
