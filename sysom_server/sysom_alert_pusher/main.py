# -*- coding: utf-8 -*- #
"""
Time                2023/11/23 19:11
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                ssh.py
Description:
"""
from clogger import logger
from fastapi import FastAPI
from app.routers import health
from conf.settings import YAML_CONFIG
from sysom_utils import CmgPlugin, SysomFramework
from app.executor import AlertListener


app = FastAPI()

app.include_router(health.router, prefix="/api/v1/alert_pusher/health")
# app.include_router(health.router, prefix="/api/v1/alert_pusher/person")


#############################################################################
# Write your API interface here, or add to app/routes
#############################################################################


def init_framwork():
    SysomFramework\
        .init(YAML_CONFIG) \
        .load_plugin_cls(CmgPlugin) \
        .start()
    logger.info("SysomFramework init finished!")


@app.on_event("startup")
async def on_start():
    init_framwork()
    
    #############################################################################
    # Perform some microservice initialization operations over here
    #############################################################################
    
    AlertListener().start()


@app.on_event("shutdown")
async def on_shutdown():
    pass