sysommonit_items = {
    "sysom_proc_cpu_total":["mode","instance"],
    "sysom_proc_cpus":["mode","instance","cpu_name"],
    "sysom_proc_sirq":["type","instance"],
    "sysom_proc_stat_counters":["counter","instance"],
    "sysom_proc_meminfo":["value","instance"],
    "sysom_proc_vmstat":["value","instance"],
    "sysom_proc_self_statm":["value","instance"],
    "sysom_proc_networks":["instance","counter","network_name"],
    "sysom_proc_disks":["disk_name","instance","counter"],
    "sysom_proc_pkt_status":["counter","instance"],
    "sysom_fs_stat":["mount","fs","instance","counter"],
    "sysom_sock_stat":["value","instance"],
    "sysom_proc_schedstat":["cpu","instance","value"],
    "sysom_proc_loadavg":["value","instance"],
    "sysom_proc_buddyinfo":["value","instance"],
    "sched_moni_jitter":["instance","mod","value"],
    "sysom_net_health_hist":["value","instance"],
    "sysom_net_health_count":["value","instance"],
    "sysom_net_retrans_count":["value","instance"],
    "sysom_uptime":["value","instance"],
    "sysom_cgroups":["type","instance","value"],
    "sysom_softnets":["cpu","instance","value"],
    "sysom_net_ip_count":["value","instance"],
    "sysom_net_icmp_count":["value","instance"],
    "sysom_net_udp_count":["value","instance"],
    "sysom_net_tcp_count":["value","instance"],
    "sysom_net_tcp_ext_count":["value","instance"]

    }

sysommonit_items_general_name = {
    "load":"sysom_proc_loadavg-load1",
    }
